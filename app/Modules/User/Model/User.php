<?php
namespace App\Modules\User\Model;

use App\Exceptions\Handler;
use App\Modules\User\Controllers\UserController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomDBException;
use League\Flysystem\Exception;
use App\Interfaces\ModuleModelInterface;

/**
 * Класс для работы с пользователями
 *
 * @category Laravel_Modules
 * @package  App\Modules\User\Controllers
 * @author   Kukanov Oleg <speed.live@mail.ru>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://crm.lets-code.ru/
 */
class User extends Model implements ModuleModelInterface
{

    /**
     * Имя используемой таблицы
     *
     * @var string
     */
    public $table = 'module_users';

    /**
     * Поля таблицы, доступные для выборки
     *
     * @var array
     */
    protected static $fields = [
        'id', 'login', 'email', 'sort', 'created_at', 'updated_at',
        'name', 'surname', 'middle_name'
    ];

    public $fillable = [
        'id', 'login', 'email', 'sort', 'password', 'created_at', 'updated_at',
        'name', 'surname', 'middle_name'
    ];



    /**
     * Вернёт поля таблицы, доступные для выборки
     *
     * @return array
     */
    public static function fields()
    {
        // TODO: Implement fields() method.
        return self::$fields;
    }


    /**
     * Вернёт группы пользователя
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(
            'App\Modules\User\Model\Group',
            'module_users_to_group', 'user_id', 'group_id'
        );
    }

    public function companies()
    {
        return $this->belongsToMany(
            'App\Modules\Companies\Model\Company',
            'module_companies_users', 'user_id', 'company_id'
        );
    }


    /**
     * Проверят может ли текущий
     * пользователь выполнять действие $action
     *
     * @param string $action         - символьный код действия
     * @param bool   $ThrowException - выбрасывать ли исключение
     *
     * @return bool
     *
     * @throws CustomDBException
     */
    public static function can($action, $ThrowException = false)
    {
        return UserController::can($action, $ThrowException);
    }

    /**
     * Обёртка для функции isAdmin у UserController
     *
     * @param string $login - логин пользователя
     * @param bool   $json  - формат ответа
     *
     * @return bool|mixed
     */
    public static function isAdmin($login, $json = false)
    {
        $UC = new UserController();
        return $UC->isAdmin($login, $json);
    }

    /**
     * Валидатор входных данных для добавления нового пользователя
     *
     * @param array $data - данные пользователя
     *
     * @return bool
     * @throws CustomValidationException
     */
    public static function userValidator($data)
    {
        if (!$data['sort']) {
            $data['sort'] = 100;
        }
        $validatorRules = [
            'login' => 'required|alpha_dash|max:255|unique:module_users,login',
            'email' => 'required|email|max:255|unique:module_users,email',
            'sort' => 'required|integer',
            'name' => 'nullable|min:1|max:100',
            'surname' => 'nullable|min:1|max:100',
            'middle_name' => 'nullable|min:1|max:100'
        ];
        //'email' => 'unique:users,email_address,10'
        //условие срабатывает, если мы пытаемся обновить запись
        if (array_key_exists('update_id', $data) && $data['update_id']) {
            $validatorRules['login'] = $validatorRules['login'].
                ','.$data['update_id'];
            $validatorRules['email'] = $validatorRules['email'].
                ','.$data['update_id'];
        }

        if ($data['password']) {
            $validatorRules['password'] = 'required';
            $validatorRules['password_confirm'] = 'required|same:password';
        }

        $validator = Validator::make((array)$data, $validatorRules);

        if ($validator->fails()) {
            throw new CustomValidationException(
                $validator,
                'User data validation error',
                [
                    'data' => $data,
                    'rules' => $validatorRules
                ]
            );
        } else {
            return true;
        }
    }

}
