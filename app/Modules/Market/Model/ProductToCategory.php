<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с отношением между товарами и категориями
 *
 * @package App\Modules\Market\Model
 */
class ProductToCategory extends BaseModel
{
    public $table = 'module_market_product_category';

    public $timestamps = true;

    public $fillable = [
        'id',
        'product_id',
        'category_id'
    ];

    public $rules = [
        'product_id'  => 'required|integer|min:1|max:4294967295',
        'category_id' => 'required|integer|min:1|max:4294967295'
    ];

    public function product()
    {
        return $this->belongsTo('App\Modules\Market\Model\Product', 'product_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Modules\Market\Model\Category', 'category_id');
    }

}