<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с корзиной
 *
 * @package App\Modules\Market\Model
 */
class Discount extends BaseModel
{
    public $table = 'module_market_discount';

    public $timestamps = false;

    public $fillable = [
        'id',
        'type',      //тип скидки (процентная/фактическая)
        'value',     //размер скидки
        'active',    //активность
        'company_id' //id компании
    ];

    public $rules = [
        'type'       => 'required|integer|min:0|max:10',
        'value'      => 'nullable|numeric|min:0',
        'active'     => 'required|integer|min:0|max:10',
        'company_id' => 'required|integer|min:1|max:4294967295'
    ];

    public static $accessors = [
        'symbol', 'name'
    ];

    /**
     *  Получение символа для отображения
     *
     * @param BaseModel $instance - экземпляр класса Discount
     *
     * @return string
     */
    public function getSymbolAttr(BaseModel $instance)
    {
        return ((int) $instance->attributes['type'] === 1) ? '%' : 'р';
    }

    /**
     * Получение названия скидки
     *
     * @param BaseModel $instance - экземпляр класса Discount
     *
     * @return null|string
     */
    public function getNameAttr(BaseModel $instance)
    {
        return 'Скидка ' . $instance->attributes['value']
            . ' ' . $instance->attributes['symbol']
            . ' (id: ' . $instance->attributes['id'] . ')';
    }

    /**
     * Связь с компанией
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Modules\Companies\Model\Company', 'company_id');
    }

    /**
     * Связь c товарами
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(
            'App\Modules\Market\Model\Product',
            'module_market_product_discount', 'discount_id', 'product_id'
        );
    }
}