<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;
use App\Classes\MarketHelper as MH;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с корзиной
 *
 * @package App\Modules\Market\Model
 */
class Order extends BaseModel
{
    public $table = 'module_market_order';

    public $timestamps = true;

    public $fillable = [
        'id',
        'user_id',          //id пользователя
        'user_name',        //имя пользователя
        'user_phone',       //номер телефона пользователя
        'comment',          //комментарий к заказу
        'delivery_type_id', //id типа доставки
        'delivery_id',      //id доставки компании (указывается, если тип доставки = курьерская)
        'stock_id',         //id склада для самовывоза (указывается, если тип доставки = самовывоз)
        'payment_type_id',  //id способа оплаты
        'cart_id',          //id корзины заказа
        'status_id',        //id статуса заказа
        'created_at',       //дата создания
        'updated_at'        //дата обновления
    ];

    public $rules = [
        'user_id'          => 'required|integer|min:1|max:4294967295',
        'user_name'        => 'required|min:1|max:255',
        'user_phone'       => 'required|min:1|max:255',
        'comment'          => 'nullable|min:1|max:50000',
        'delivery_type_id' => 'required|integer|min:1|max:4294967295',
        'delivery_id'      => 'nullable|integer|min:1|max:4294967295',
        'stock_id'         => 'nullable|integer|min:1|max:4294967295',
        'payment_type_id'  => 'required|integer|min:1|max:4294967295',
        'cart_id'          => 'nullable|integer|min:1|max:4294967295',
        'status_id'        => 'nullable|integer|min:1|max:4294967295'
    ];

    /**
     * Связь с статусом заказа
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Modules\Billing\Model\OrderStatus', 'status_id');
    }

    /**
     * Связь с пользователем
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Modules\Users\Model\User', 'user_id');
    }

    /**
     * Связь с типом доставки
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryType()
    {
        return $this->belongsTo('App\Modules\Delivery\Model\DeliveryType', 'delivery_type_id');
    }

    /**
     * Связь с доставкой компании
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function delivery()
    {
        return $this->belongsTo('App\Modules\Delivery\Model\CompanyDelivery', 'delivery_id');
    }

    /**
     * Связь с складом
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stock()
    {
        return $this->belongsTo('App\Modules\Stock\Model\Stock', 'stock_id');
    }

    /**
     * Связь с способом оплаты
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentType()
    {
        return $this->belongsTo('App\Modules\Billing\Model\PaymentType', 'payment_type_id');
    }

    /**
     * Связь с корзиной
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo('App\Modules\Market\Model\Cart', 'cart_id');
    }


}