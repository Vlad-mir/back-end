<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с категорями магазина
 *
 * @package App\Modules\Market\Model
 */
class Category extends BaseModel
{
    public $table = 'module_market_category';

    public $timestamps = false;

    public $fillable = [
        'id',
        'name',
        'code',
        'sort',
        'parent_category_id'
    ];

    public $rules = [
        'name'               => 'required|min:1|max:255',
        'code'               => 'required|min:1|max:255|unique:module_market_category,code',
        'sort'               => 'required|integer|min:0|max:999',
        'parent_category_id' => 'nullable|integer|min:1|max:4294967295'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Modules\Market\Model\Category', 'parent_category_id');
    }

}


















