<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с избанными товарами
 *
 * @package App\Modules\Market\Model
 */
class Favorite extends BaseModel
{
    public $table = 'module_market_favorite';

    public $timestamps = false;

    public $fillable = [
        'id',
        'user_id',    //id пользователя
        'product_id', //id товара
        'sku_id'      //id СКУ
    ];

    public $rules = [
        'user_id'    => 'required|integer|min:1|max:4294967295',
        'product_id' => 'required|integer|min:1|max:4294967295',
        'sku_id'     => 'required|integer|min:1|max:4294967295'
    ];

    /**
     * Связь с пользователем
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Modules\User\Model\User', 'user_id');
    }

    /**
     * Связь с товаром
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Modules\Market\Model\Product', 'product_id');
    }

    /**
     * Связь с SKU
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function sku()
    {
        return $this->belongsTo('App\Modules\Market\Model\SKU', 'sku_id');
    }

}