<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с товарами магазина
 *
 * @package App\Modules\Market\Model
 */
class Product extends BaseModel
{
    public $table = 'module_market_product';

    public $timestamps = true;

    public $fillable = [
        'id',
        'name',      //название товара
        'code',      //символьный код
        'sort',      //сортировка
        'public',    //флаг публикации/модерации (0 - добавлен, 1 - на модерации, 2 - опубликован, 3 - скрыт)
        'company_id' //id компании, к которой относится товар
    ];

    public $rules = [
        'name'       => 'required|min:1|max:255',
        'code'       => 'required|min:1|max:255|unique:module_market_product,code',
        'sort'       => 'required|integer|min:0|max:999',
        'public'     => 'nullable|min:0|max:2',
        'company_id' => 'required|integer|min:1|max:4294967295'
    ];

    /**
     * Связь товара с компанией
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Modules\Companies\Model\Company', 'company_id');
    }

    /**
     * Связь товара с категориями
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function category()
    {
        return $this->belongsToMany(
            'App\Modules\Market\Model\Category',
            'module_market_product_category', 'product_id', 'category_id'
        );
    }

    /**
     * Связь товара с СКУ
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function sku()
    {
        return $this->hasMany('App\Modules\Market\Model\SKU', 'product_id');
    }

    /**
     * Связь товара с дефолтным СКУ
     *
     * @return mixed
     */
    public function defaultSku()
    {
        return $this->hasOne('App\Modules\Market\Model\SKU', 'product_id')->where('default', 1);
    }

    /**
     * Связь c акциями
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discounts()
    {
        return $this->belongsToMany(
            'App\Modules\Market\Model\Discount',
            'module_market_product_discount', 'product_id', 'discount_id'
        );
    }

    /**
     * Добавление нескольких скидок к товару
     *
     * @param Product      $Product            - Экземпляр модели товара
     * @param array|string $discounts          - id скидки
     * @param boolean      $removeOldDiscounts - Нужно ли удалять старые скидки
     *
     * @throws CustomException
     */
    public function attachProductDiscounts(Product $Product, $discounts, $removeOldDiscounts = false)
    {
        if (is_string($discounts)) {
            $discounts = json_decode($discounts, true);
        } elseif(!is_array($discounts)) {
            throw new CustomException(
                ['product' => $Product, 'discounts' => $discounts],
                [], 400, 'Некорректный формат списка скидок'
            );
        }
        if ($removeOldDiscounts) {
            $Product->discounts()->detach();
        }
        foreach ($discounts as $discount) {
            $this->attachProductDiscount($Product, $discount);
        }
    }

    /**
     * Добавляет скидку к товару
     *
     * @param Product $Product     - Экземпляр модели товара
     * @param integer $discount_id - id скидки
     *
     * @return Product
     */
    public function attachProductDiscount(Product $Product, $discount_id)
    {
        $Discount = Discount::where('id', $discount_id)->first();
        if ($Discount) {
            $Product->discounts()->attach($Discount);
        }

        return $Product;
    }

    /**
     * Добавление товара в несколько категорий
     *
     * @param Product      $Product           - Экземпляр модели товара
     * @param array|string $categories        - Массив из id категорий
     * @param boolean      $removeOldCategory - Нужно ли удалять старые категории
     *
     * @throws CustomException
     */
    public function pushProductToCategories(
        Product $Product, $categories, $removeOldCategory = false
    ) {
        if (is_string($categories)) {
            $categories = json_decode($categories, true);
        } elseif(!is_array($categories)) {
            throw new CustomException(
                ['product' => $Product, 'categories' => $categories],
                [], 400, 'Некорректный формат списка категорий'
            );
        }
        if ($removeOldCategory) {
            $Product->category()->detach();
        }
        foreach ($categories as $category) {
            $this->pushProductToOneCategory($Product, $category);
        }
    }

    /**
     * Добавляет товар в одну категорию
     *
     * @param Product $Product     - Экземпляр модели товара
     * @param integer $category_id - id категории
     *
     * @return Product
     * @throws CustomException
     */
    public function pushProductToOneCategory(Product $Product, $category_id)
    {
        $Category = Category::where('id', $category_id)->first();
        if ($Category) {
            $Product->category()->attach($Category);
        }

        return $Product;
    }

    /**
     * Удаляет товар из нескольких категорий
     *
     * @param Product      $Product    - Экземпляр модели товара
     * @param array|string $categories - Массив из id категорий
     *
     * @throws CustomException
     */
    public function removeProductFromCategories(Product $Product, $categories)
    {
        if (is_string($categories)) {
            $categories = json_decode($categories, true);
        } elseif(!is_array($categories)) {
            throw new CustomException(
                ['product' => $Product, 'categories' => $categories],
                [], 400, 'Некорректный формат списка категорий'
            );
        }

        foreach ($categories as $category) {
            $this->removeProductFromOneCategory($Product, $category);
        }
    }

    /**
     * Удаляет товар из одной категории
     *
     * @param Product $Product     - Экземпляр модели товара
     * @param integer $category_id - id категории
     *
     * @return Product
     * @throws CustomException
     */
    public function removeProductFromOneCategory(Product $Product, $category_id)
    {
        $Category = Category::where('id', $category_id)->first();
        if ($Category) {
            $Product->category()->detach($category_id);
        }

        return $Product;
    }

}