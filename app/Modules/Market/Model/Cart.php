<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;
use App\Classes\MarketHelper as MH;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с корзиной
 *
 * @package App\Modules\Market\Model
 */
class Cart extends BaseModel
{
    public $table = 'module_market_cart';

    public $timestamps = true;

    public $fillable = [
        'id',
        'code',           //символьный код корзины
        'user_id',        //id пользователя
        'status',         //статус корзины от 1 до 9
        'price',          //конечная цена без скидки
        'discount',       //размер скидки
        'discount_price', //конечная цена со скидкой
        'amount',         //количество товара в корзине
        'created_at',     //дата создания
        'updated_at'      //дата обновления
    ];

    public $rules = [
        'code'    => 'required|min:1|max:255',
        'user_id' => 'nullable|integer|min:1|max:4294967295',
        'price'   => 'nullable|numeric|min:0',
        'status'  => 'required|integer|min:0|max:10',
        'discount_price' => 'nullable|numeric|min:0',
        'discount'       => 'nullable|numeric|min:0',
        'amount'         => 'nullable|integer|min:1|max:4294967295',
    ];

    /**
     * Связь с пользователем
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Modules\User\Model\User', 'user_id');
    }

    /**
     * Связь с товарами в корзине
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Modules\Market\Model\ProductInCart', 'cart_id');
    }

    /**
     * Обёрткая для функции в MH
     *
     * @param $cart_id
     * @return mixed
     */
    public static function calc($cart_id) {
        return MH::calcCart($cart_id);
    }

}