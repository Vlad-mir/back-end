<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с товарами в корзине
 *
 * @package App\Modules\Market\Model
 */
class ProductInCart extends BaseModel
{
    public $table = 'module_market_product_cart';

    public $timestamps = false;

    public $fillable = [
        'id',                  // id записи
        'product_id',          // id товара из каталога
        'sku_id',              // id торгового предложения
        'cart_id',             // id корзины
        'price',               // цена единицы товара
        'discount',            // размер скидки на единицу товара в рублях
        'discount_price',      // цена с учётом скидки на единицу товара
        'full_price',          // цена с учётом количества товара без скидки
        'full_discount_price', // цена с учётом количества товара и скидки
        'full_discount',       // размер скидки на все товары в рублях
        'amount',              // количество товара
        'date',                // дата и время добавления товара в корзину
    ];

    public $rules = [
        'product_id'          => 'required|integer|min:1|max:4294967295',
        'sku_id'              => 'required|integer|min:1|max:4294967295',
        'cart_id'             => 'required|integer|min:1|max:4294967295',
        'price'               => 'required|numeric|min:0',
        'discount'            => 'nullable|numeric|min:0',
        'discount_price'      => 'required|numeric|min:0',
        'full_price'          => 'required|numeric|min:0',
        'full_discount_price' => 'required|numeric|min:0',
        'full_discount'       => 'required|numeric|min:0',
        'amount'              => 'required|integer|min:1|max:4294967295',
        'date'                => 'nullable'
    ];

    /**
     * Связь с товаром
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Modules\Market\Model\Product', 'product_id');
    }

    /**
     * Связь с торговым предложением
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sku()
    {
        return $this->belongsTo('App\Modules\Market\Model\SKU', 'sku_id');
    }

    /**
     * Связь с корзиной
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo('App\Modules\Market\Model\Cart', 'cart_id');
    }

}