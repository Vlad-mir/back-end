<?php

namespace App\Modules\Market\Model;

use App\Classes\BaseModel;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;
use App\Http\Controllers\State;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Модель для работы с торговыми предложениями
 *
 * @package App\Modules\Market\Model
 */
class SKU extends BaseModel
{
    public $table = 'module_market_sku';

    public $timestamps = false;

    public $fillable = [
        'id',
        'name',          //название
        'description',   //описание товара
        'code',          //символьный код
        'sort',          //сортировка
        'status',        //флаг публикации/модерации
        'product_id',    //id товара
        'price',         //конечная цена без скидки за единицу продукции
        'default',       //флаг стандартного СКУ (1 - стандартное, 0 - остальные)
        'hero_image_id', //id файла главноего изображения
    ];

    public $rules = [
        'name'          => 'required|min:1|max:255',
        'description'   => 'nullable|min:1|max:50000',
        'code'          => 'required|min:1|max:255|unique:module_market_sku,code',
        'sort'          => 'required|integer|min:1|max:4294967295',
        'status'        => 'nullable|integer|min:0|max:10',
        'product_id'    => 'required|integer|min:1|max:4294967295',
        'price'         => 'required|numeric|min:0',
        'default'       => 'required|integer|min:0|max:1',
        'hero_image_id' => 'required|integer|min:1|max:4294967295'
    ];

    /**
     * Связь с изображением
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heroImage()
    {
        return $this->belongsTo('App\Modules\Properties\Model\Files', 'hero_image_id');
    }

    /**
     * Связь с товаром
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Modules\Market\Model\Product', 'product_id');
    }

    /**
     * Связь SKU с остатками
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remnants()
    {
        return $this->hasMany('App\Modules\Stock\Model\Remnants', 'sku_id');
    }

    /**
     * Связь со значениями свойств SKU
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propertyValues()
    {
        return $this->hasMany('App\Modules\Properties\Model\PropertiesValues', 'entity_id');
    }

    /**
     * Связь с категориями товара
     * к которому относится SKU
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(
            'App\Modules\Market\Model\ProductToCategory', 'product_id', 'product_id'
        );

    }

    /**
     * Проверяет принадлежит ли товар, к компании
     * авторизированного пользователя
     *
     * @param integer $product_id    - id товара
     * @param boolean $TrowException - флаг выбрасывания исключения
     *
     * @return bool
     * @throws CustomException
     */
    public static function checkSKUAccess($product_id, $TrowException = false)
    {
        $findUser = false;

        if (!$product_id) {
            return $findUser;
        }

        $State = State::getInstance();
        $User = $State->getUser();

        if (User::isAdmin($User['id'], true)) {
            return true;
        }

        $Product = Product::where(Product::table() . '.id', $product_id)
            ->with(['company' => function($q) use ($User) {
                $q->with(['users' => function ($q) use ($User) {
                    $q->where('module_users.id', $User['id']);
                }]);
            }])->first();

        if (isset($Product['company']['users'])) {
            $users = $Product['company']['users'];
            foreach ($users as $user) {
                if ($user['id'] === $User['id']) {
                    $findUser = true;
                }

            }
        }

        if ($TrowException && !$findUser) {
            throw new CustomException([], [], 403);
        }

        return $findUser;
    }
}