<?php
//публичные методы
Route::group(
    [
        'namespace'  => 'App\Modules\Market\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','cors']
    ],
    function () {

        /* Категории товаров каталога */

        Route::get('/category',          ['uses' => 'CategoryController@getCategories'    ]);
        Route::get('/category/{id}',     ['uses' => 'CategoryController@getCategoryById'  ]);
        Route::get('/category/tree/get', ['uses' => 'CategoryController@getTreeCategories']);

        /* Товары каталога */

        Route::get('/product',      ['uses' => 'ProductController@getProducts'   ]);
        Route::get('/product/{id}', ['uses' => 'ProductController@getProductById']);

        /* Торговые предложения товара */

        Route::get('/sku',      ['uses' => 'SKUController@getSKUs'   ]);
        Route::get('/sku/{id}', ['uses' => 'SKUController@getSKUById']);
        Route::get('/sku/{id}/product', ['uses' => 'SKUController@getProductBySKUId']);

        /* Корзина товаров */

        Route::get( '/cart',      ['uses' => 'CartController@getCarts'   ]);
        Route::get( '/cart/{id}', ['uses' => 'CartController@getCartById']);
        Route::post('/cart',      ['uses' => 'CartController@postCart'   ]);
        Route::put( '/cart',      ['uses' => 'CartController@putCart'    ]);
        Route::get( '/user_cart', ['uses' => 'CartController@getUserCart']);

        /* Товары в корзине */

        Route::get(   '/product_in_cart',      ['uses' => 'ProductCartController@getProductsInCarts'     ]);
        Route::post(  '/product_in_cart',      ['uses' => 'ProductCartController@postProductInCart'      ]);
        Route::put(   '/product_in_cart',      ['uses' => 'ProductCartController@putProductInCart'       ]);
        Route::get(   '/product_in_cart/{id}', ['uses' => 'ProductCartController@getProductInCartById'   ]);
        Route::delete('/product_in_cart/{id}', ['uses' => 'ProductCartController@deleteProductInCartById']);

        Route::delete('/cart/{cart_id}/sku/{sku_id}', ['uses' => 'ProductCartController@deleteCartSKUById']);
        Route::get(   '/cart/{cart_id}/sku/{sku_id}', ['uses' => 'ProductCartController@getCartSKUById'   ]);
        Route::get(   '/cart/{cart_id}/sku',          ['uses' => 'ProductCartController@getCartSKUs'   ]);

        /* Скидки на товар */

        Route::get('/discount',      ['uses' => 'DiscountController@getDiscounts'   ]);
        Route::get('/discount/{id}', ['uses' => 'DiscountController@getDiscountById']);

        /* Заказы */

        Route::get(   '/order',      ['uses' => 'OrderController@getOrders'      ]);
        Route::get(   '/order/{id}', ['uses' => 'OrderController@getOrderById'   ]);
        Route::post(  '/order',      ['uses' => 'OrderController@postOrder'      ]);
    }
);

//защищённые методы
Route::group(
    [
        'namespace'  => 'App\Modules\Market\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','auth','cors']
    ],
    function () {

        /* Категории товаров каталога */

        Route::post(  '/category',          ['uses' => 'CategoryController@postCategory'      ]);
        Route::put(   '/category',          ['uses' => 'CategoryController@putCategory'       ]);
        Route::delete('/category/{id}',     ['uses' => 'CategoryController@deleteCategoryById']);

        /* Товары каталога */

        Route::post(  '/product',      ['uses' => 'ProductController@postProduct'      ]);
        Route::put(   '/product',      ['uses' => 'ProductController@putProduct'       ]);
        Route::delete('/product/{id}', ['uses' => 'ProductController@deleteProductById']);
        Route::get('/user/{id}/products', ['uses' => 'SKUController@getUserProducts']);

        /* Торговые предложения товара */

        Route::post(  '/sku',      ['uses' => 'SKUController@postSKU'      ]);
        Route::put(   '/sku',      ['uses' => 'SKUController@putSKU'       ]);
        Route::delete('/sku/{id}', ['uses' => 'SKUController@deleteSKUById']);
        Route::delete('/sku/{id}/image/{img_id}', ['uses' => 'SKUController@deleteSKUHeroImage']);

        /* Корзина товаров */

        Route::delete('/cart/{id}', ['uses' => 'CartController@deleteCartById']);

        /* Скидки на товар */

        Route::post(  '/discount',      ['uses' => 'DiscountController@postDiscount'      ]);
        Route::put(   '/discount',      ['uses' => 'DiscountController@putDiscount'       ]);
        Route::delete('/discount/{id}', ['uses' => 'DiscountController@deleteDiscountById']);

        /* Заказы */

        Route::put(   '/order',      ['uses' => 'OrderController@putOrder'       ]);
        Route::delete('/order/{id}', ['uses' => 'OrderController@deleteOrderById']);

        /* Избранные товары */

        Route::get(   '/favorite',                ['uses' => 'FavoriteController@getFavorites'       ]);
        Route::post(  '/favorite',                ['uses' => 'FavoriteController@postFavorite'       ]);
        Route::get(   '/favorite/{id}',           ['uses' => 'FavoriteController@getFavoriteById'    ]);
        Route::delete('/favorite/{id}',           ['uses' => 'FavoriteController@deleteFavoriteById' ]);
        Route::delete('/user/{user_id}/favorite', ['uses' => 'FavoriteController@deleteUserFavorites']);
    }
);
