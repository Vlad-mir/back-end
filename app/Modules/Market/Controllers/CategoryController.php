<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Market\Model\Category;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с категорями
 *
 * @package App\Modules\Category\Controllers
 */
class CategoryController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новую категорию
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postCategory(Request $request)
    {
        User::can('market_postcategory', true);
        $Category = Category::post($request, ['parent_category_id' => 'parent']);

        if ($Category) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_postcategory',
                null, 'category', $Category->id,
                ['data' => self::modelFilter($Category, Category::fields())]
            );
        }

        return parent::response($request->all(), $Category, 200);
    }

    /**
     * Изменяет существующую категорию
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putCategory(Request $request)
    {
        User::can('market_putcategory', true);
        $Category = ['old' => false, 'new' => false];
        $Category = Category::put($request, ['parent_category_id' => 'parent']);

        if (isset($Category['old']) && isset($Category['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_putcategory',
                null, 'category', $Category['new']->id,
                ['data' => self::modelFilter($Category['new'], Category::fields())],
                [$Category['old'], $Category['new']]
            );
        }

        return parent::response($request->all(), $Category['new'], 200);
    }

    /**
     * Возвращает категорию по id
     *
     * @param int  $id   - id категории
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getCategoryById($id, $json = true)
    {
        User::can('market_viewcategory', true);

        $Category = Category::where('id', $id)->first();
        if (!$Category) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $Category;
        } else {
            return parent::response(['id' => $id], $Category, 200);
        }
    }

    /**
     * Возвращает категории по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getCategories(Request $request)
    {
        User::can('market_viewcategory', true);
        $result = parent::dbGet(new Category(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Возвращает дерево категорий товаров
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getTreeCategories(Request $request) {
        User::can('market_viewcategory', true);
        $Categories = [];
        $CategoriesList = parent::dbGet(new Category(), $request, [], [], true);
        $CategoriesList = $CategoriesList->orderBy('sort', 'asc')->get()->toArray();

        foreach ($CategoriesList as $Category) {
            if ($Category['parent_category_id']) {
                $Categories[ $Category['parent_category_id'] ][] = $Category;
            } else {
                $Categories[0][] = $Category;
            }
        }
        $Categories = $this->buildTree($Categories, 0);

        return parent::response($request->all(), $Categories, 200);
    }


    /**
     * Строит дерево категорий товаров
     *
     * @param array   $Categories - Массив категорий
     * @param integer $parent_id  - ID родительской категории
     *
     * @return array
     */
    private function buildTree ($Categories, $parent_id) {
        if (is_array($Categories) && isset($Categories[$parent_id])) {
            $tree = [];
            foreach($Categories[$parent_id] as $Category) {
                $tree[] = [
                    'self'  => $Category,
                    'child' => $this->buildTree($Categories, $Category['id'])
                ];
            }
        } else return [];

        return $tree;
    }


    /**
     * Удаляет категории по id
     *
     * @param int $id - id кстегории
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteCategoryById($id)
    {
        User::can('market_deletecategory', true);

        $Category = Category::where('id', $id)->first();
        if (!$Category) {
            throw new CustomException(['id' => $id], [], 404);
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deletecategory',
            null, 'category', $Category->id,
            ['data' => self::modelFilter($Category, Category::fields())]
        );

        return parent::response(['id' => $id], $Category->delete(), 200);

    }

}