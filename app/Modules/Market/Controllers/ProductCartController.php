<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\State;
use App\Modules\Market\Model\Cart;
use App\Modules\Market\Model\ProductInCart;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;
use App\Classes\MarketHelper as MH;

/**
 * Класс для работы с категорями
 *
 * @package App\Modules\Category\Controllers
 */
class ProductCartController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет запись товара в корзине
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postProductInCart(Request $request)
    {
        $FindCart = CartController::checkUserCart($request->get('cart_code'));
        $request->merge(['cart_id' => $FindCart['id']]);
        $Cart = [];
        $userId = $request->get('user_id');

        //Если в запросе есть id пользователя, а в корзине его нет,
        //То допишем id в корзину (значи пользовател добавил несколько товаров
        // будучи не авторизованным, а потом авторизовался)
        if ($userId && !$FindCart->user_id) {
            $FindCart->user_id = $userId;
            $FindCart->save();
        }

        //если такая запись в корзине уже есть, то обновляем её
        if ($IssetEntity = $this->getProductInCartByData($request)) {
            $request->merge(['id' => $IssetEntity['id']]);
            return $this->putProductInCart($request);
        }

        $data = MH::calcProductData(
            $request->get('sku_id'),
            $FindCart['id'],
            $request->get('amount')
        );
        $request->replace($data);


        $ProductInCart = ProductInCart::post($request, [
            'product_id' => 'product',
            'cart_id'    => 'cart',
            'sku_id'     => 'sku'
        ]);

        if ($ProductInCart) {
            $Cart = Cart::calc($FindCart['id']);
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_postproductincart',
                null, 'productincart', $ProductInCart->id,
                ['data' => self::modelFilter($ProductInCart, ProductInCart::fields())]
            );
        }

        return parent::response($request->all(), $Cart, 200);
    }

    /**
     * Ищет в корзине товар с переданными SKU id и cart id
     *
     * @param Request $request - запрос от клиента
     *
     * @return mixed
     */
    public function getProductInCartByData(Request $request)
    {
        return ProductInCart::where('cart_id', $request->get('cart_id'))
            ->where('sku_id', $request->get('sku_id'))
            ->first();
    }

    /**
     * Изменяет существующую запись в корзине
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putProductInCart(Request $request)
    {

        CartController::checkUserCart($request->get('cart_id'));

        $data = MH::calcProductData(
            $request->get('sku_id'),
            $request->get('cart_id'),
            $request->get('amount')
        );
        $data['id'] = $request->get('id');
        $request->replace($data);


        $ProductInCart = ['old' => false, 'new' => false];
        $ProductInCart = ProductInCart::put($request, [
            'product_id' => 'product',
            'cart_id'    => 'cart',
            'sku_id'     => 'sku'
        ]);

        if (isset($ProductInCart['old']) && isset($ProductInCart['new'])) {

            $Cart = Cart::calc($request->get('cart_id'));

            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_putproductincart',
                null, 'productincart', $ProductInCart['new']->id,
                ['data' => self::modelFilter($ProductInCart['new'], ProductInCart::fields())],
                [$ProductInCart['old'], $ProductInCart['new']]
            );
        }

        return parent::response($request->all(), $Cart, 200);
    }

    /**
     * Возвращает запись по id
     *
     * @param int  $id   - id категории
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getProductInCartById($id, $json = true)
    {
        User::can('market_viewproductincart', true);

        $ProductInCart = ProductInCart::where('id', $id)
            ->with(['product', 'cart', 'sku'])->first();
        if (!$ProductInCart) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $ProductInCart;
        } else {
            return parent::response(['id' => $id], $ProductInCart, 200);
        }
    }

    /**
     * Возвращает записи в корзине по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getProductsInCarts(Request $request)
    {
        User::can('market_viewproductincart', true);
        $result = parent::dbGet(new ProductInCart(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет СКУ из корзины по id
     *
     * @param int  $id   - id кстегории
     * @param bool $json - формат ответа
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteProductInCartById($id, $json = true)
    {
        User::can('market_deleteproductincart', true);

        $ProductInCart = $this->getProductInCartById($id, false);
        $cart_id = $ProductInCart->cart_id;

        $Cart = CartController::checkUserCart($cart_id);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deleteproductincart',
            null, 'productincart', $ProductInCart->id,
            ['data' => self::modelFilter($ProductInCart, ProductInCart::fields())]
        );

        $ProductInCart->delete();

        Cart::calc($cart_id);

        return ($json) ? parent::response(['id' => $id], $ProductInCart, 200) : $Cart;
    }


    /**
     * Удаляет товар из корзины
     * по id коризны и id SKU
     *
     * @param Request $request   - Запрос от клиента
     * @param integer $cart_code - код корзины
     * @param integer $sku_id    - id СКУ
     * @param boolean $json      - формат ответа
     *
     * @return mixed
     */
    public function deleteCartSKUById(Request $request, $cart_code, $sku_id, $json = true)
    {
        $Cart = CartController::checkUserCart($cart_code, true);
        $request->replace(['cart_id' => (int) $Cart['id'], 'sku_id'  => (int) $sku_id]);
        $ProductCart = $this->getProductInCartByData($request);
        $this->deleteProductInCartById($ProductCart->id, false);

        $request->merge(['cart_code' => $cart_code]);
        $Cart = CartController::call('getUserCart', $request, false);

        return ($json) ? parent::response(['cart_code' => $cart_code, 'sku_id' => $sku_id], $Cart, 200) : $Cart;
    }

    /**
     * Возвоащает SKU в корзине
     * по id коризны и id SKU
     *
     * @param Request $request - Запрос от клиента
     * @param integer $cart_id - id корзины
     * @param integer $sku_id  - id СКУ
     * @param boolean $json    - формат ответа
     *
     * @return mixed
     */
    public function getCartSKUById(Request $request, $cart_id, $sku_id, $json = true)
    {
        CartController::checkUserCart($cart_id);

        $request->replace([
            'cart_id' => $cart_id,
            'sku_id'  => $sku_id
        ]);

        $ProductCart = $this->getProductInCartByData($request);

        return ($json) ? parent::response(['cart_id' => $cart_id, 'sku_id' => $sku_id], $ProductCart, 200) : $ProductCart;
    }

    /**
     * Возвращает все СКУ из корзины с переданным id
     *
     * @param integer $cart_id - id корзины
     * @param boolean $json    - формат ответа
     *
     * @return mixed
     */
    public function getCartSKUs($cart_id, $json = true)
    {
        CartController::checkUserCart($cart_id);

        $ProductCart = ProductInCart::where('cart_id', $cart_id)->get();

        return ($json) ? parent::response(['cart_id' => $cart_id, ], $ProductCart, 200) : $ProductCart;
    }
}