<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\State;
use App\Modules\Billing\Model\OrderStatus;
use App\Modules\Billing\Model\PaymentType;
use App\Modules\Delivery\Model\CompanyDelivery;
use App\Modules\Delivery\Model\DeliveryType;
use App\Modules\Market\Model\Cart;
use App\Modules\Market\Model\Order;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\Stock\Model\Stock;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с типами доставки
 *
 * @package App\Modules\Market\Controllers
 */
class OrderController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый заказ
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function postOrder(Request $request)
    {
        User::can('market_postorder', true);

        $User = State::User();
        if (!$User) {
            throw new CustomException($request->all(), [], 401);
        }
        $request->merge(['user_id' => $User['id']]);

        $Order = Order::post($request, [
            'delivery_type_id' => 'deliveryType',
            'delivery_id'      => 'delivery',
            'stock_id'         => 'stock',
            'payment_type_id'  => 'paymentType',
            'cart_id'          => 'cart',
            'status_id'        => 'status'
        ]);

        if ($Order) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_postorder',
                null, 'order', $Order->id,
                ['data' => self::modelFilter($Order, Order::fields())]
            );
        }

        return parent::response($request->all(), $Order, 200);
    }

    /**
     * Изменяет существующий заказ
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putOrder(Request $request)
    {
        User::can('market_putorder', true);
        $Order = ['old' => false, 'new' => false];
        $Order = Order::put($request, [
            'delivery_type_id' => 'deliveryType',
            'delivery_id'      => 'delivery',
            'stock_id'         => 'stock',
            'payment_type_id'  => 'paymentType',
            'cart_id'          => 'cart',
            'status_id'        => 'status'
        ]);

        if (isset($Order['old']) && isset($Order['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_putorder',
                null, 'order', $Order['new']->id,
                ['data' => self::modelFilter($Order['new'], Order::fields())],
                [$Order['old'], $Order['new']]
            );
        }

        return parent::response($request->all(), $Order['new'], 200);
    }

    /**
     * Возвращает заказ по id
     *
     * @param int  $id   - id заказа
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getOrderById($id, $json = true)
    {
        User::can('market_vieworder', true);

        $Order = Order::where('id', $id)
            ->with([
                'deliveryType', 'delivery', 'stock', 'paymentType', 'status',
                'cart' => function($q) {
                    $q->with([
                        'products' => function($q) {
                            $q->with(['sku']);
                        }
                    ]);
                }
            ])->first();
        if (!$Order) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $Order;
        } else {
            return parent::response(['id' => $id], $Order, 200);
        }
    }

    /**
     * Возвращает заказы по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getOrders(Request $request)
    {
        User::can('market_vieworder', true);
        $result = parent::dbGet(new Order(), $request, [], [
            'deliveryType' => new DeliveryType(),
            'delivery'     => new CompanyDelivery(),
            'stock'        => new Stock(),
            'paymentType'  => new PaymentType(),
            'cart'         => new Cart(),
            'status'       => new OrderStatus()
        ]);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет заказ по id
     *
     * @param int $id - id заказа
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteOrderById($id)
    {
        User::can('market_deleteorder', true);

        $Order = $this->getOrderById($id, false);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deleteorder',
            null, 'order', $Order->id,
            ['data' => self::modelFilter($Order, Order::fields())]
        );

        return parent::response(['id' => $id], $Order->delete(), 200);

    }

}