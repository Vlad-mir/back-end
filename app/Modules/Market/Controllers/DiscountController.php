<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Market\Model\Category;
use App\Modules\Market\Model\Discount;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с товарами
 *
 * @package App\Modules\Discount\Controllers
 */
class DiscountController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    public $types = [
        1, //процентная
        2  //фактическая
    ];

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Проверяет корректность типа скидки
     *
     * @param Request $request - Запрос от клиента
     *
     * @return bool
     * @throws CustomException
     */
    public function checkType($request) {
        $type = $request->get('type');
        if (!in_array($type, $this->types)) {
            throw new CustomException(
                $request->all(), [], 400,
                'Тип скидки указан неверно'
            );
        }

        return true;
    }

    /**
     * Добавляет новый товар
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postDiscount(Request $request)
    {
        User::can('market_postdiscount', true);
        $data = $request->only(Discount::fields());

        $this->checkType($request);

        $Discount = Discount::post($request, ['company_id' => 'company']);

        if ($Discount) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_postdiscount',
                null, 'discount', $Discount->id,
                ['data' => self::modelFilter($Discount, Discount::fields())]
            );
        }

        return parent::response($request->all(), $Discount, 200);
    }

    /**
     * Изменяет существующий товар
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putDiscount(Request $request)
    {
        User::can('market_putdiscount', true);
        $data = $request->only(Discount::fields());
        $Discount = ['old' => false, 'new' => false];
        $this->checkType($request);

        $Discount = Discount::put($request, ['company_id' => 'company']);

        if (isset($Discount['old']) && isset($Discount['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_putdiscount',
                null, 'discount', $Discount['new']->id,
                ['data' => self::modelFilter($Discount['new'], Discount::fields())],
                [$Discount['old'], $Discount['new']]
            );
        }

        return parent::response($request->all(), $Discount['new'], 200);
    }

    /**
     * Возвращает товар по id
     *
     * @param int  $id   - id товары
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getDiscountById($id, $json = true)
    {
        User::can('market_viewdiscount', true);

        $Discount = Discount::where('id', $id)
            ->with(['company'])->first();
        if (!$Discount) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $Discount;
        } else {
            return parent::response(['id' => $id], $Discount, 200);
        }
    }

    /**
     * Возвращает товары по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getDiscounts(Request $request)
    {
        User::can('market_viewdiscount', true);
        $result = Discount::getAccessorsValues(Discount::get($request));
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет товары по id
     *
     * @param int $id - id кстегории
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteDiscountById($id)
    {
        User::can('market_deletediscount', true);

        $Discount = Discount::where('id', $id)->first();
        if (!$Discount) {
            throw new CustomException(['id' => $id], [], 404);
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deletediscount',
            null, 'discount', $Discount->id,
            ['data' => self::modelFilter($Discount, Discount::fields())]
        );

        return parent::response(['id' => $id], $Discount->delete(), 200);

    }
}