<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\State;
use App\Modules\Companies\Controllers\CompanyController;
use App\Modules\Logger\Model\Logger;
use App\Modules\Market\Model\Category;
use App\Modules\Market\Model\Discount;
use App\Modules\Market\Model\Product;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\Market\Model\SKU;
use App\Modules\User\Controllers\UserController;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с товарами
 *
 * @package App\Modules\Product\Controllers
 */
class ProductController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый товар
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postProduct(Request $request)
    {
        User::can('market_postproduct', true);
        $data = $request->only(array_merge(Product::fields(), ['categories', 'discounts']));

        if (!$data['public']
            || $data['public'] === '0'
            || $data['public'] === 'undefined'
        ) {
            $request->merge(['public' => 0]);
        }

        $Product = Product::post($request, ['company_id' => 'company']);

        if ($Product) {
            if ($data['categories']) {
                $Product->pushProductToCategories($Product, $data['categories'], true);
            }
            if ($data['discounts']) {
                $Product->attachProductDiscounts($Product, $data['discounts'], true);
            }

            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_postproduct',
                null, 'product', $Product->id,
                ['data' => self::modelFilter($Product, Product::fields())]
            );
        }

        $Product = $this->getProductById($Product->id, false);

        return parent::response($request->all(), $Product, 200);
    }

    /**
     * Изменяет существующий товар
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putProduct(Request $request)
    {
        User::can('market_putproduct', true);
        $data = $request->only(array_merge(Product::fields(), ['categories', 'discounts']));
        $Product = ['old' => false, 'new' => false];

        if (!$data['public']
            || $data['public'] === '0'
            || $data['public'] === 'undefined'
        ) {
            $request->merge(['public' => 0]);
        }

        $Product = Product::put($request, ['company_id' => 'company']);

        if (isset($Product['old']) && isset($Product['new'])) {
            if (isset($data['categories'])) {
                $Product['new']->pushProductToCategories($Product['new'], $data['categories'], true);
            }

            if (isset($data['discounts'])) {
                $Product['new']->attachProductDiscounts($Product['new'], $data['discounts'], true);
            }

            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_putproduct',
                null, 'product', $Product['new']->id,
                ['data' => self::modelFilter($Product['new'], Product::fields())],
                [$Product['old'], $Product['new']]
            );
        }

        $Product['new'] = $this->getProductById($Product['new']->id, false);

        return parent::response($request->all(), $Product['new'], 200);
    }

    /**
     * Возвращает товар по id
     *
     * @param int  $id   - id товары
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getProductById($id, $json = true)
    {
        User::can('market_viewproduct', true);

        $Product = Product::where('id', $id)
            ->with([
                'category', 'company',
                'discounts', 'sku' => function ($q) {
                    $q->with('remnants', 'heroImage')
                        ->orderBy('default', 'desc')
                        ->orderBy('sort');
                },
            ])->first();
        if (!$Product) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $Product;
        } else {
            return parent::response(['id' => $id], $Product, 200);
        }
    }

    /**
     * Возвращает товары по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     * @param bool    $json    - флаг отправки json
     *
     * @return mixed
     */
    public function getProducts(Request $request, $json = true)
    {
        User::can('market_viewproduct', true);
        $result = parent::dbGet(new Product(), $request, [], [
            'discounts'  => new Discount(),
            'defaultSku' => [
                'model' => new SKU(), 'with' => ['heroImage', 'remnants']
            ]
        ]);
        return ($json) ? parent::response($request->all(), $result, 200) : $result;
    }
    
    /**
     * Удаляет товары по id
     *
     * @param int $id - id кстегории
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteProductById($id)
    {
        User::can('market_deleteproduct', true);

        $Product = Product::where('id', $id)->first();
        if (!$Product) {
            throw new CustomException(['id' => $id], [], 404);
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deleteproduct',
            null, 'product', $Product->id,
            ['data' => self::modelFilter($Product, Product::fields())]
        );


        //удаляем id у лога
        $Events = Logger::where('log_action','market_postproduct')
            ->where('entity_id', $id)
            ->where('entity_type', 'product')->first();
        if ($Events) {
            $Events->entity_id = null;
            $Events->save();
        }

        return parent::response(['id' => $id], $Product->delete(), 200);

    }

}