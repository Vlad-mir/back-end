<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Market\Model\Favorite;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\Market\Model\Product;
use App\Modules\Market\Model\SKU;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\State;


/**
 * Класс для работы с избранными товарами
 *
 * @package App\Modules\Category\Controllers
 */
class FavoriteController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Проверяяет есть ли у текущего пользователя
     * доступ к товару в избранном
     *
     * @param integer $favorite_id - id избранного товара
     *
     * @return bool
     * @throws CustomException
     */
    public static function checkAccess($favorite_id)
    {
        $User = State::User();
        if (User::isAdmin($User['id'], true)) {
            return true;
        }

        $self = new self();
        $Favorite  = $self->getFavoriteById($favorite_id, false);

        if (isset($Favorite['user']['id'])
            || $Favorite['user']['id'] != $favorite_id
        ) {
            throw new CustomException(
                [], [], 403,
                'Товар находится в избранном не у текущего пользователя. ' .
                'Попробуйте войти на сайт заного.'
            );
        }

        return true;
    }
    
    /**
     * Добавляет товар в избранное
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postFavorite(Request $request)
    {
        User::can('market_postfavorite', true);

        if (!$request->get('user_id')) {
            $User = State::User();
            $request->merge(['user_id' => $User['id']]);
        }

        $Favorite = Favorite::post($request, [
            'user_id'    => 'user',
            'product_id' => 'product',
            'sku_id'     => 'sku'
        ]);

        if ($Favorite) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_postfavorite',
                null, 'favorite', $Favorite->id,
                ['data' => self::modelFilter($Favorite, Favorite::fields())]
            );
        }

        return parent::response($request->all(), $Favorite, 200);
    }

    /**
     * Возвращает избранные товары по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getFavorites(Request $request)
    {
        User::can('market_viewfavorite', true);

        $result = parent::dbGet(new Favorite(), $request, [], [
            'user'    => new User(),
            'product' => new Product(),
            'sku'     => new SKU()
        ]);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Возвращает избанный товар по id
     *
     * @param int  $id   - id избранного товара
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getFavoriteById($id, $json = true)
    {
        User::can('market_viewfavorite', true);

        $Favorite = Favorite::where('id', $id)
            ->with(['user', 'product', 'sku'])->first();
        if (!$Favorite) {
            throw new CustomException(['id' => $id], [], 404);
        }

        return ($json) ? parent::response(['id' => $id], $Favorite, 200) : $Favorite;
    }

    /**
     * Удаляет товар из избранного
     *
     * @param int $id - id товара
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteFavoriteById($id)
    {
        User::can('market_deletefavorite', true);
        self::checkAccess($id);
        $Favorite = $this->getFavoriteById($id, false);
        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deletefavorite',
            null, 'favorite', $Favorite->id,
            ['data' => self::modelFilter($Favorite, Favorite::fields())]
        );

        return parent::response(['id' => $id], $Favorite->delete(), 200);
    }

    /**
     * Удаляет все избранные товары у
     * пользователя с переданным id
     *
     * @param integer $user_id - id пользователя
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteUserFavorites($user_id)
    {
        User::can('market_deleteallfavorite', true);

        $User = State::User();
        if (!User::isAdmin($User['id'], true) && $user_id != $User['id']) {
            throw new CustomException(
                [], [], 403, 'Вы не можете удалять ' .
                'избранные товары других пользователей'
            );
        }

        $Favorite = [];

        LoggerController::write(
            $this->getModuleName(), 'market_deleteallfavorite', null, 'favorite'
        );

        return parent::response(['user_id' => $user_id], $Favorite->delete(), 200);
    }
}