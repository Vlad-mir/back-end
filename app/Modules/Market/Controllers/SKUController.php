<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\State;
use App\Modules\Companies\Controllers\CompanyController;
use App\Modules\Market\Model\Product;
use App\Modules\Market\Model\ProductToCategory;
use App\Modules\Market\Model\SKU;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\Properties\Controllers\FileController;
use App\Modules\Properties\Model\Files;
use App\Modules\Properties\Model\PropertiesValues;
use App\Modules\User\Controllers\UserController;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с товарами
 *
 * @package App\Modules\SKU\Controllers
 */
class SKUController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    public $statuses = [
        1, //не опубликованно
        2, //на модерации
        3  //опубликованно
    ];

    /**
     * Проверяет корректность статуса
     *
     * @param Request $request - Запрос от клиента
     *
     * @return bool
     * @throws CustomException
     */
    public function checkStatus(Request $request)
    {
        $status = $request->get('status');
        if (!in_array($status, $this->statuses)) {
            throw new CustomException(
                $request->all(), [], 400,
                'Статус СКУ указан неверно'
            );
        }

        return true;
    }

    /**
     * Добавляет новое торговое предложение
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function postSKU(Request $request)
    {
        User::can('market_postsku', true);
        $ProductId = $request->get('product_id');

        //Проверяем существование товара
        Product::exist($ProductId, true);

        //Проверим, может ли пользователь добавлять СКУ к этому товару
        SKU::checkSKUAccess($ProductId, true);

        //Проверим корректность статуса
        $this->checkStatus($request);

        //добавляем файл к СКУ
        if ($request->hasFile('hero_image')) {
            $file = self::upFile($request, 'hero_image');
            $request->merge(['hero_image_id' => $file['id']]);
        }

        $result = SKU::post(
            $request, [
                'product_id' => 'product',
                'hero_image_id' => 'heroImage'
            ]
        );

        return parent::response($request->all(), $result, 200);
    }

    /**
     * Изменяет существующее торговое предложение
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putSKU(Request $request)
    {
        User::can('market_putsku', true);
        $SKU = ['old' => false, 'new' => false];
        $ProductId = $request->get('product_id');

        //Проверяем существование товара
        Product::exist($ProductId, true);

        //Проверим, может ли пользователь добавлять СКУ к этому товару
        SKU::checkSKUAccess($ProductId, true);

        //Проверим корректность статуса
        $this->checkStatus($request);

        //добавляем файл к СКУ
        if ($request->hasFile('hero_image')) {
            $file = self::upFile($request, 'hero_image');
            if (!$file || !isset($file['id']) || !$file['id']) {
                throw new CustomException(
                    $request->all(), [], 500,
                    'Не удалось сохранить файл, пожалуйста обратитесь в тех поддержку.'
                );
            }
            $request->merge(['hero_image_id' => $file['id']]);
        }


        $SKU = SKU::put($request, ['product_id' => 'product']);
        if (isset($SKU['old']) && isset($SKU['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_putsku',
                null, 'sku', $SKU['new']->id,
                ['data' => self::modelFilter($SKU['new'], SKU::fields())],
                [$SKU['old'], $SKU['new']]
            );

            if ($request->hasFile('hero_image')) {
                //удаляем старый файл
                FileController::call('deleteFileById', $SKU['old']['hero_image_id'], false);
            }

        }
        return parent::response($request->all(), $SKU['new'], 200);
    }

    /**
     * Возвращает торговое предложение по id
     *
     * @param int  $id   - id торгового предложения
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getSKUById($id, $json = true)
    {
        User::can('market_viewsku', true);

        $SKU = SKU::where('id', $id)->with([
            'heroImage', 'remnants', 'categories',
            'product' => function($q) {
                $q->with([
                    'category', 'company',
                    'discounts' => function ($q) {
                        $q->where('active', 1);
                    },
                ]);
            }
        ])->first();
        if (!$SKU) {
            throw new CustomException(
                ['id' => $id], [], 404,
                'Торговое предложение с id "' . $id . '" не найдено'
            );
        }

        return ($json) ? parent::response(['id' => $id], $SKU, 200) : $SKU;
    }

    /**
     * Возвращает торговые предложения по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     * @param boolean $json    - Формат ответа
     *
     * @return mixed
     */
    public function getSKUs(Request $request, $json = true)
    {
        User::can('market_viewsku', true);
        $result = parent::dbGet(new SKU(), $request, [], [
            'product'   => [
                'model' => new Product(),
                'with'  => ['category', 'company', 'discounts']
            ],
            'heroImage'      => new Files(),
            'categories'     => new ProductToCategory(),
            'propertyValues' => new PropertiesValues(),
        ]);
        return ($json) ? parent::response($request->all(), $result, 200) : $result;
    }

    /**
     * Удаляет торговое предложение по id
     *
     * @param int $id - id торгового предложения
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteSKUById($id)
    {
        User::can('market_deletesku', true);

        $SKU = $this->getSKUById($id, false);

        if ($SKU->default) {
            throw new CustomException(
                ['id' => $id], [], 404,
                'Невозможно удалить торговое предложение, назначенное по умолчанию.'
            );
        }

        $ProductId = $SKU->product->id;

        //Проверяем существование товара
        Product::exist($ProductId, true);

        //Проверим, может ли пользователь добавлять/удалять СКУ к/у этому товару
        SKU::checkSKUAccess($ProductId, true);

        //удаляем файл картинки
        FileController::call('deleteFileById', $SKU->hero_image_id, false);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deletesku',
            null, 'sku', $SKU->id,
            ['data' => self::modelFilter($SKU, SKU::fields())]
        );

        return parent::response(['id' => $id], $SKU->delete(), 200);

    }

    /**
     * Возвращает товар по id торгового предложения
     *
     * @param $id - id SKU
     *
     * @return mixed
     * @throws CustomException
     */
    public function getProductBySKUId($id)
    {
        $SKU = $this->getSKUById($id, false);
        if (!$SKU->product) {
            throw new CustomException(
                ['id' => $id], [], 404, 'Товар не найден.'
            );
        }

        $Product = Product::where('id', $SKU->product->id)
            ->with([
                'category', 'company', 'discounts',
                'sku' => function ($q) {
                $q->with('remnants', 'heroImage')
                    ->orderBy('default', 'desc')
                    ->orderBy('sort');
            }
            ])->first();

        return parent::response(['id' => $id], $Product, 200);
    }

    /**
     * Возвращает товары, принадлежащие пользователю
     * с переданным id
     *
     * @param null|int $id - id пользователя
     *
     * @return mixed
     */
    public function getUserProducts(Request $request, $id = null)
    {

        $CurrentUser = State::User();
        $CurrentUser = UserController::call('getUserById', $CurrentUser['id'], true);
        $CurrentUserCompanies = $CurrentUser->companies()->get()->pluck('id')->toArray();

        $result = []; $filter = '{"relation":[';
        $Companies = CompanyController::call('getUserCompanies', $id, false);
        if(count($Companies)) {
            $Companies = $Companies->pluck('id');
        } else {
            $Companies = [0];
        }

        foreach ($Companies as $i => $Company) {
            if ($i) {
                $filter .= ',"OR",';
            }
            $filter .= '["product","company_id","=","' . $Company . '"]';
        } $filter .= '],"model":[["default","=","1"]]}';
        $request->merge(['filter' => $filter]);

        $result = $this->getSKUs($request, false)->toArray();

        if (isset($result['data'])) {$resultTemp = $result['data'];}
        else {$resultTemp = $result;}

        foreach ($resultTemp as &$item) {
            $item['owner'] = (in_array($item['product']['company_id'], $CurrentUserCompanies)) ? true : false;
        } unset($item);

        if (isset($result['data'])) {$result['data'] = $resultTemp;}
        else {$result = $resultTemp;}


        return parent::response([
            'request' => $request->all(),
            'query'   => ['id' => $id],
            'companies' => $Companies
        ], $result, 200);
    }

    /**
     * Удаляет главное изображение у СКУ
     *
     * @param integer $sku_id   - SKU ID
     * @param integer $image_id - ID изображения
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteSKUHeroImage($sku_id, $image_id)
    {
        $SKU = $this->getSKUById($sku_id, false);

        if ((int) $SKU->hero_image_id !== (int) $image_id) {
            throw new CustomException(
                ['sku_id' => $sku_id, 'image_id' => $image_id], [], 400,
                "Изображение с id '" . $image_id .
                "' не принадлежит СКУ с id '" . $sku_id . "'"
            );
        }

        //удаляем файл картинки
        FileController::call('deleteFileById', $SKU->hero_image_id, false);
        $SKU->heroImage()->dissociate();

        return parent::response([
            'sku_id' => $sku_id,
            'image_id' => $image_id
        ], $SKU, 200);
    }
}