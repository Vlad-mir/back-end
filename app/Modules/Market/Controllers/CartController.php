<?php

namespace App\Modules\Market\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Market\Model\Cart;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\Market\Model\ProductInCart;
use App\Modules\Properties\Model\Properties;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\State;


/**
 * Класс для работы с корзиной
 *
 * @package App\Modules\Category\Controllers
 */
class CartController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Market';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Статусы корзины
     * дизайн БД допускает 9 статусов:
     * от 1 до 9
     *
     * @var array
     */
    public $cart_statuses = [
        1, //Создана не авторизированным пользователем
        2, //Создана авторизированным пользователем
        3, //Завершена (пользователь оформил заказ)
        4, //Статус не задействован
        5, //Статус не задействован
        6, //Статус не задействован
        7, //Статус не задействован
        8, //Статус не задействован
        9  //Статус не задействован
    ];

    /**
     * Проверяет может ли пользователь
     * оперировать с переданной корзиной
     *
     * @param integer|string $cart   - id или код корзины корзины
     * @param boolean        $byCode - флаг, определяющий как искать корзину
     *
     * @return bool
     * @throws CustomException
     */
    public static function checkUserCart($cart, $byCode = false)
    {
        $User = State::User();
        $self = new self();
        if (is_numeric($cart) && !$byCode) {
            $Cart = $self->getCartById($cart, false);
        } else {
            $Cart = $self->getCartByCode($cart, false);
        }
        if (User::isAdmin($User['id'], true)) return $Cart;

        //если корзина привязанна к пользвателю
        //и он не текущий пользователь
        if (isset($Cart['user']['id'])
            && $Cart['user']['id'] != $User['id']
        ) {
            throw new CustomException(
                [], [], 403,
                'Корзина не принадлежит текущему пользователю. ' .
                'Попробуйте очистить корзину, войти на сайт заново и оформить заказ.'
            );
        }
        return $Cart;
    }

    /**
     * Проверяет корректность переданного статуса
     *
     * @param Request $request - Запрос от клиента
     *
     * @return bool
     * @throws CustomException
     */
    public function checkStatus(Request $request)
    {
        $status = $request->get('status');
        if (!in_array($status, $this->cart_statuses)) {
            throw new CustomException(
                $request->all(), [], 400,
                'Статус корзины указан неверно'
            );
        }

        return true;
    }

    /**
     * Генерирует код для корзины
     *
     * @param integer $user_id - id пользователя, для которого создаётся корзина
     *
     * @return string
     */
    public function getCartCode($user_id = 0)
    {
        return md5($user_id . time() . env('ENCRYPT_SALT', '4feM'));
    }

    /**
     * Возвращает корзину пользователя
     *
     * @param Request $request
     * @param boolean $json
     *
     * @return mixed
     */
    public function getUserCart(Request $request, $json = true)
    {
        $cartCore = $request->get('cart_code');
        $User = State::User();
        if ($cartCore) { //если указан код корзины
            $Cart = Cart::where('code', $cartCore);
        } elseif ($User) { //если кода нет, но пользователь авторизован
            $Cart = Cart::where('user_id', $User['id'])
                ->where('status', 2);
        } else {
            //если пользователь не авторизован и нету кода
            //то создаём новую коризну
            return $this->postCart($request);
        }


        //получаем товары в корзине
        $Cart = $Cart->with(['products' => function ($q) {
            $q->with(['product',
                //получаем СКУ, которое пользователь добавил в корзину
                'sku' => function ($q) {
                    //получаем картинку и значения свойств СКУ
                    $q->with(['heroImage',
                        'propertyValues' => function ($q) {
                            //выбираем только те свойства, которые должны отображаться в фильтре
                            $q->whereHas('property', function ($q) {
                                $q->where('show_in_filter', 1);
                            })->with([
                                'property' => function ($q) {
                                    $q->select(
                                        'module_properties.id',
                                        'module_properties.name',
                                        'module_properties.show_in_filter'
                                    );
                                },
                                'choices' => function ($q) {
                                    $q->select(
                                        'module_properties_choices.id',
                                        'module_properties_choices.name',
                                        'module_properties_choices.code',
                                        'module_properties_choices.property_id'
                                    );
                                }
                            ]);
                            //выбираем их с самим свойством и значениями свойств
                        }
                    ]);
                }
            ]);
        }])->first();



        $Cart = json_decode(json_encode($Cart), true);

        if (isset($Cart['products']) && count($Cart['products'])) {
            foreach ($Cart['products'] as &$product) {
                if (isset($product['sku']['property_values']) && count($product['sku']['property_values'])) {
                    foreach ($product['sku']['property_values'] as &$value) {
                        $value['choices'] = self::arrayToValueKey('code', $value['choices']);
                        if (isset($value['choices'][ $value['value'] ]['name'])) {
                            $value['label'] = $value['choices'][$value['value']]['name'];
                        }
                        unset($value['choices']);
                    } unset($value);
                }
            } unset($product);
        }

        if (!$Cart) {
            return $this->postCart($request);
        }

        return ($json) ? parent::response($request->all(), $Cart, 200) : $Cart;
    }

    /**
     * Добавляет новую корзину
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postCart(Request $request)
    {
        $User = State::User();

        //генерируем код для корзины
        $request->replace([
            'code'     => $this->getCartCode(($User ? $User['id'] : 0)),
            'user_id'  => ($User ? $User['id'] : null),
            'status'   => ($User ? 2 : 1),
            'price'    => 0,
            'discount' => 0,
            'discount_price' => 0
        ]);

        //проверяем статус
        $this->checkStatus($request);

        $Cart = Cart::post($request, ['user_id' => 'user']);

        if ($Cart) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_postcart',
                null, 'cart', $Cart->id,
                ['data' => self::modelFilter($Cart, Cart::fields())]
            );
        }

        return parent::response($request->all(), $Cart, 200);
    }

    /**
     * Изменяет существующую корзину
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putCart(Request $request)
    {
        $this->checkStatus($request);

        $cart_id = ($request->get('id')) ? $request->get('id') : $request->get('update_id');
        self::checkUserCart($cart_id);

        $Cart = ['old' => false, 'new' => false];
        $Cart = Cart::put($request, ['user_id' => 'user']);

        if (isset($Cart['old']) && isset($Cart['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'market_putcart',
                null, 'cart', $Cart['new']->id,
                ['data' => self::modelFilter($Cart['new'], Cart::fields())],
                [$Cart['old'], $Cart['new']]
            );
        }

        return parent::response($request->all(), $Cart['new'], 200);
    }

    /**
     * Возвращает корзину по id
     *
     * @param int  $id   - id корзины
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getCartById($id, $json = true)
    {
        $Cart = Cart::where('id', $id)
            ->with([
                'user',
                'products' => function($q) {
                    $q->with(['product',
                        'sku' => function ($q) {
                            $q->with('heroImage');
                        }
                    ]);
                }
            ])->first();
        if (!$Cart) {
            throw new CustomException(
                request()->all(), [], 404,
                'Корзина с id "' . $id . '" не найдена'
            );
        }

        return ($json) ? parent::response(request()->all(), $Cart, 200) : $Cart;
    }

    /**
     * Возвращает корзину по её коду
     *
     * @param string  $code - code корзины
     * @param bool    $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getCartByCode($code, $json = true)
    {
        $Cart = Cart::where('code', $code)
            ->with([
                'user',
                'products' => function($q) {
                    $q->with([
                        'product',
                        'sku' => function ($q) {
                            $q->with('heroImage');
                        }
                    ]);
                }
            ])->first();
        if (!$Cart) {
            throw new CustomException(
                request()->all(), [], 404,
                'Корзина с кодом "' . $code . '" не найдена'
            );
        }
        return ($json) ? parent::response(request()->all(), $Cart, 200) : $Cart;
    }

    /**
     * Возвращает корзины по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getCarts(Request $request)
    {
        $result = parent::dbGet(new Cart(), $request, [], ['user' => new User()]);
        return parent::response($request->all(), $result, 200);
    }
    
    /**
     * Удаляет корзину по id
     *
     * @param int $id - id корзины
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteCartById($id)
    {
        User::can('market_deletecart', true);
        self::checkUserCart($id);
        $Cart = $this->getCartById($id, false);
        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'market_deletecart',
            null, 'cart', $Cart->id,
            ['data' => self::modelFilter($Cart, Cart::fields())]
        );

        return parent::response(['id' => $id], $Cart->delete(), 200);

    }

}