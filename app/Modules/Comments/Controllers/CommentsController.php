<?php

namespace App\Modules\Comments\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\State;
use App\Modules\Comments\Model\Comments;
use App\Modules\Comments\Model\CommentsFile;
use App\Modules\Logger\Controllers\LoggerController;
use App\Modules\Module\Controllers\ModuleController;
use App\Modules\Properties\Controllers\FileController;
use App\Modules\Properties\Model\Files;
use App\Modules\Module\Model\Module;
use App\Modules\Module\Controllers\ModuleController as ModuleCTR;
use App\Modules\User\Controllers\UserController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Exceptions\CustomDBException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;


/**
 * Класс для работы с контроллерами
 *
 * @category Laravel_Modules
 * @package  App\Modules\User\Controllers
 * @author   Kukanov Oleg <speed.live@mail.ru>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://crm.lets-code.ru/
 */
class CommentsController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Comments';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый комментарий
     * Параметры POST запроса:
     * text - текст комментария
     * entity - название сущности (task, user и тд)
     * entity_id - id сущности
     * module_code - код модуля, к которому принадлежит сущности
     * file - массив файлов (имя поля должно быть "file[]")
     *
     * @param Request $request - Экземпляр Request
     *
     * @return mixed
     * @throws CustomException
     */
    public function postComment(Request $request)
    {
        $data = $request->only(
            'text', 'entity', 'entity_id', 'module_code', 'file'
        );

        User::can('comments_post_comments', true);

        //найдём модуль, который просит пользователь
        $Module = new Module();
        //метод выбросит исключение с ошибкой 404, если не найдёт
        //поэтому обрабатывать его не нужно
        $module = $Module->getModuleByCode($data['module_code']);
        $data['module_id'] = $module['id'];


        $State = State::getInstance();
        $CurrentUser = $State->getUser();
        $data['user_id'] = $CurrentUser['id'];

        //проверим, существует ли запрашиваемая сущность в конфигах
        $ModuleCTR = new ModuleCTR();
        if (!$ModuleCTR->issetEntity($data['module_code'], $data['entity'])) {
            throw new CustomException(
                $data, [], 400,
                'Сущность "'.$data['entity'].'" ' .
                'не найдена в модуле с кодом "'.$data['module_code'].'"'
            );
        }


        $Comments = new Comments();
        $validate = $Comments->commentValidator($data);
        if ($validate) {
            $Comments->text = $data['text'];
            $Comments->entity = $data['entity'];
            $Comments->entity_id = $data['entity_id'];
            $Comments->module_id = $data['module_id'];
            $Comments->user()->associate($data['user_id']);
            $Comments->save();

            $files = $request->file('file');
            if (count($files) && $Comments) {
                $FileCTR = new FileController();
                $fileArray = [];
                foreach ($files as $file) {
                    $uploadFile = $FileCTR->upload($file);
                    $relationFile = new CommentsFile();
                    $relationFile->file()->associate($uploadFile['id']);
                    $relationFile->comment()->associate($Comments->id);
                    $relationFile->save();
                    $fileArray[] = $relationFile;
                }
                $Comments->files = $fileArray;
            }

            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'comments_post_comments',
                null, 'comments', $Comments->id,
                ['data' => self::modelFilter($Comments, Comments::fields())]
            );

        }

        return parent::response($data, $Comments, 200);
    }

    /**
     * Добавляет новый комментарий
     * Параметры POST запроса:
     * id - ID обновляемого комментария
     * text - текст комментария
     * entity - название сущности (task, user и тд)
     * entity_id - id сущности
     * module_code - код модуля, к которому принадлежит сущности
     * file - массив файлов (имя поля должно быть "file[]")
     * _method - метод передачи, необходимо отправлять POST запрос, сюда передать PUT
     *
     * @param Request $request - Экземпляр Request
     *
     * @return mixed
     * @throws CustomException
     */
    public function putComment(Request $request)
    {
        $data = $request->only(
            'id', 'text', 'entity', 'entity_id', 'module_code', 'file'
        );

        User::can('comments_put_comments', true);

        //проверим, передали ли id комментария для редактирования
        if (!$data['id']) {
            throw new CustomException(
                $data, [], 400,
                'Параметра "ID" не определён в запросе'
            );
        }
        $data['update_id'] = $data['id'];

        //проверим, существует ли такой комментарий
        $Comments = Comments::where('id', $data['id'])->first();
        if (!$Comments) {
            throw new CustomException(
                $data, [], 404,
                'Комментарий с id '.$data['id'].' не найден'
            );
        }

        //получим текущего пользователя
        $State = State::getInstance();
        $CurrentUser = $State->getUser();
        $data['user_id'] = $CurrentUser['id'];

        //если текущий пользователь не автор комментария и не админ, то исключение
        if ($CurrentUser['id'] != $Comments->user_id && !$CurrentUser['isAdmin'] ) {
            throw new CustomException(
                $data, [], 403, CustomException::text('403')
            );
        }


        //найдём модуль, который просит пользователь
        $Module = new Module();
        //метод выбросит исключение с ошибкой 404, если не найдёт
        //поэтому обрабатывать его не нужно
        $module = $Module->getModuleByCode($data['module_code']);
        $data['module_id'] = $module['id'];


        //проверим, существует ли запрашиваемая сущность в конфигах
        $ModuleCTR = new ModuleCTR();
        if (!$ModuleCTR->issetEntity($data['module_code'], $data['entity'])) {
            throw new CustomException(
                $data, [], 400,
                'Сущность "'.$data['entity'].'" ' .
                'не найдена в модуле с кодом "'.$data['module_code'].'"'
            );
        }

        $validate = $Comments->commentValidator($data);
        if ($validate) {
            $Comments->text = $data['text'];
            $Comments->entity = $data['entity'];
            $Comments->entity_id = $data['entity_id'];
            $Comments->module_id = $data['module_id'];
            $Comments->user()->associate($data['user_id']);
            $Comments->save();

            $files = $request->file('file');
            if (count($files) && $Comments) {
                $FileCTR = new FileController();
                $fileArray = [];
                foreach ($files as $file) {
                    $uploadFile = $FileCTR->upload($file);
                    $relationFile = new CommentsFile();
                    $relationFile->file()->associate($uploadFile['id']);
                    $relationFile->comment()->associate($Comments->id);
                    $relationFile->save();
                    $fileArray[] = $relationFile;
                }
                $Comments->files = $fileArray;
            }

            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'comments_put_comments',
                null, 'comments', $Comments->id,
                ['data' => self::modelFilter($Comments, Comments::fields())]
            );

        }

        return parent::response($data, $Comments, 200);
    }

    /**
     * Удаляет комментарий с переданным id
     *
     * @param integer $id - id комментария для удаления
     *
     * @return mixed
     * @throws CustomException
     */
    public function removeComment($id)
    {
        User::can('comments_delete_comments', true);

        //проверим, передали ли id комментария для удаления
        if (!$id) {
            throw new CustomException(
                ['id' => $id], ['id' => $id], 400,
                'Параметр "ID" не определён в запросе'
            );
        }

        //проверим, существует ли такой комментарий
        $Comments = Comments::where('id', $id)->first();
        if (!$Comments) {
            throw new CustomException(
                ['id' => $id], [], 404,
                'Комментарий с id '.$id.' не найден'
            );
        }

        //получим текущего пользователя
        $State = State::getInstance();
        $CurrentUser = $State->getUser();

        //если текущий пользователь не автор комментария и не админ, то исключение
        if ($CurrentUser['id'] != $Comments->user_id && !$CurrentUser['isAdmin'] ) {
            throw new CustomException(
                ['id' => $id], [], 403, CustomException::text('403')
            );
        }

        //получим файлы, прикреплённые к комментарию
        $Files = $Comments->files();
        $IssetFiles = $Files->get();
        //если они есть, то удаляем их
        if ($IssetFiles) {
            //удаляем сами файлы с диска и из таблицы файлов
            $FileController = new FileController();
            foreach ($IssetFiles->pluck('file_id') as $fileId) {
                $FileController->deleteFileById($fileId, false);
            }
            //удаляем записи о файлах из связующей таблицы
            $Files->delete();
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'comments_delete_comments',
            null, 'comments', $Comments->id,
            ['data' => self::modelFilter($Comments, Comments::fields())]
        );

        return parent::response(['id' => $id], $Comments->delete(), 200);
    }


    /**
     * Удаляет комментарий с переданным id
     *
     * @param integer $id      - id комментария для удаления
     * @param integer $file_id - id файла для удаления (поле file_id в таблице)
     *
     * @return mixed
     * @throws CustomException
     */
    public function removeCommentFile($id, $file_id)
    {

        //проверим, передали ли id комментария и файла для удаления
        if (!$id || !$file_id) {
            throw new CustomException(
                ['id' => $id, 'file_id' => $file_id], [], 400,
                'Параметр "id" или "file_id" не определён в запросе'
            );
        }

        //проверим, существует ли такой комментарий
        $Comments = Comments::where('id', $id)->first();
        if (!$Comments) {
            throw new CustomException(
                ['id' => $id, 'file_id' => $file_id], [], 404,
                'Комментарий с id '.$id.' не найден'
            );
        }

        //получим текущего пользователя
        $State = State::getInstance();
        $CurrentUser = $State->getUser();

        //если текущий пользователь не автор комментария и не админ, то исключение
        if ($CurrentUser['id'] != $Comments->user_id && !$CurrentUser['isAdmin'] ) {
            throw new CustomException(
                ['id' => $id, 'file_id' => $file_id], [], 403, CustomException::text('403')
            );
        }

        //получим файл, для удаления
        $Files = $Comments->files()->where('file_id', $file_id);
        $IssetFile = $Files->first();
        //если они есть, то удаляем их
        if ($IssetFile) {
            //удаляем сами файлы с диска и из таблицы файлов
            $FileController = new FileController();
            $FileController->deleteFileById($IssetFile['file_id'], false);

            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'comments_delete_comments_file',
                null, 'comments', $Comments->id,
                ['data' => self::modelFilter($Comments, Comments::fields())]
            );

            return parent::response(
                ['id' => $id, 'file_id' => $file_id],
                $Files->delete(),
                200
            );

        } else {
            throw new CustomException(
                ['id' => $id, 'file_id' => $file_id], [], 400,
                'Файл с id '.$file_id.' не найден'
            );
        }

    }

    /**
     * Возвращает список комментариев
     * Список комментариев конкретной сущности
     * можно получит так:
     * ?filter=[["entity_id","=","11"], "and", ["entity","=","task"]]
     * Параметры GET запроса:
     * page - номер страницы для отображения постраничной навигации
     * count - количество элементов для отображения на странице
     * order_by - поле для сортировки (одно из полей массива ModelName::fields())
     * order_type - направление сортировки (asc/desc)
     *
     * @param Request $request - Экземпляр Request
     *
     * @return mixed
     * @throws CustomException
     */
    public function getComments(Request $request)
    {
        User::can('comments_get_comments', true);

        return parent::response(
            $request->all(),
            parent::dbGet(new Comments(), $request, [], ['user' => new User()]),
            200
        );
    }

}