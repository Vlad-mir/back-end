<?php

namespace App\Modules\Comments\Model;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomValidationException;
use App\Exceptions\CustomDBException;
use League\Flysystem\Exception;
use App\Interfaces\ModuleModelInterface;
use App\Modules\Module\Model\Module;

/**
 * Класс для работы с файлами в комментариях
 *
 * @category Laravel_Modules
 * @package  App\Modules\User\Controllers
 * @author   Kukanov Oleg <speed.live@mail.ru>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://crm.lets-code.ru/
 */
class CommentsFile extends Model implements ModuleModelInterface
{
    public $table = 'module_comments_files';
    protected static $fields = [
        'id', 'file_id', 'comment_id'
    ];
    public $timestamps = false;

    /**
     * Вернёт поля таблицы, доступные для выборки
     *
     * @return array
     */
    public static function fields()
    {
        // TODO: Implement fields() method.
        return self::$fields;
    }

    public function comment()
    {
        return $this->belongsTo('App\Modules\Comments\Model\Comments');
    }

    public function file()
    {
        return $this->belongsTo('App\Modules\Properties\Model\Files');
    }

}