<?php

namespace App\Modules\Comments\Model;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomValidationException;
use App\Interfaces\ModuleModelInterface;
use App\Modules\Module\Model\Module;
use App\Exceptions\CustomDBException;
use League\Flysystem\Exception;


/**
 * Класс для работы с комментариями
 *
 * @category Laravel_Modules
 * @package  App\Modules\User\Controllers
 * @author   Kukanov Oleg <speed.live@mail.ru>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://crm.lets-code.ru/
 */
class Comments extends Model implements ModuleModelInterface
{

    public $table = 'module_comments';
    protected static $fields = [
        'id', 'user_id', 'text',
        'entity', 'entity_id', 'module_id', 'created_at', 'updated_at'
    ];
    public $timestamps = true;


    /**
     * Вернёт поля таблицы, доступные для выборки
     *
     * @return array
     */
    public static function fields()
    {
        // TODO: Implement fields() method.
        return self::$fields;
    }




    /**
     * Валидатор входных данных для добавления
     * и обновления комментариев
     *
     * @param array $data - данные комментария
     *
     * @return bool
     * @throws CustomValidationException
     */
    public function commentValidator($data)
    {
        $validatorRules = [
            'text' => 'required',
            'entity' => 'required|alpha_dash|max:255',
            'entity_id'  => 'required|integer|max:999',
            'module_id' => 'required|integer|max:999',
            'user_id' => 'required|integer|max:999'
        ];

        //условие срабатывает, если мы пытаемся обновить запись
        if (array_key_exists('update_id', $data) && $data['update_id']) {
            //$validatorRules['code'] = $validatorRules['code'].','.$data['update_id'];
            $validatorRules['id'] = 'required|integer';
        }

        $validator = Validator::make($data, $validatorRules);

        if ($validator->fails()) {
            throw new CustomValidationException(
                $validator,
                'Ошибка заполнения, проверьте корректность значения полей',
                $data
            );
        } else {
            return true;
        }
    }


    public function user()
    {
        return $this->belongsTo('App\Modules\User\Model\User');
    }

    public function files()
    {
        return $this->hasMany(
            'App\Modules\Comments\Model\CommentsFile', 'comment_id', 'id'
        );
    }

}