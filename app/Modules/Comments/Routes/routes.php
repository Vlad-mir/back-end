<?php

Route::group(
    [
        'namespace' => 'App\Modules\Comments\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => ['web','cors']
    ],
    function () {
    }
);

Route::group(
    [
        'namespace' => 'App\Modules\Comments\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => [
            'web','auth','cors'
        ]
    ],
    function () {

        /**
         * Получает список комментариев
         */
        Route::get(
            '/comments',
            ['uses' => 'CommentsController@getComments']
        );

        /**
         * Добавляет новый комментарий к сущности
         */
        Route::post('/comments', ['uses' => 'CommentsController@postComment']);

        /**
         * Изменяет существующий комментарий у сущности
         */
        Route::put('/comments', ['uses' => 'CommentsController@putComment']);

        /**
         * Удаляет существующий комментарий у сущности
         */
        Route::delete(
            '/comments/{id}',
            ['uses' => 'CommentsController@removeComment']
        );

        /**
         * Удаляет вложеный файл у комментария
         */
        Route::delete(
            '/comments/{id}/file/{file_id}',
            ['uses' => 'CommentsController@removeCommentFile']
        );

    }
);
