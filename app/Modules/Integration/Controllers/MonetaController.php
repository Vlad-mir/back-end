<?php

namespace App\Modules\Integration\Controllers;

use App\Classes\Moneta;
use App\Http\Controllers\Controller;
use App\Interfaces\ModuleInterface;

/**
 * Класс для работы с корзиной
 *
 * @package App\Modules\Integration\Controllers
 */
class MonetaController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Integration';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }


    public function test()
    {
        $byId = Moneta::account()->main();
        return parent::response([], [
            'byId'   => $byId
        ], 200);
    }
}