<?php

namespace App\Modules\Integration\Controllers;

use App\Classes\Sypexgeo;
use App\Http\Controllers\Controller;
use App\Interfaces\ModuleInterface;

/**
 * Класс для работы с корзиной
 *
 * @package App\Modules\Integration\Controllers
 */
class SypexgeoController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Integration';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Возвращает гео данные пользователя
     *
     * @return mixed
     */
    public function getUserLocation()
    {
        return parent::response([], Sypexgeo::get(request()->ip()), 200);
    }

}