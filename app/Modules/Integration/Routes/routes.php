<?php
//публичные методы
Route::group(
    [
        'namespace'  => 'App\Modules\Integration\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','cors']
    ],
    function () {
        Route::any('/moneta/test', ['uses' => 'MonetaController@test'  ]);
        Route::any('/userip', ['uses' => 'SypexgeoController@getUserLocation'  ]);
    }
);

//защищённые методы
Route::group(
    [
        'namespace'  => 'App\Modules\Integration\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','auth','cors']
    ],
    function () {

    }
);