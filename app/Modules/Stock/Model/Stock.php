<?php

namespace App\Modules\Stock\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с точками продаж и складами
 *
 * @package App\Modules\Companies\Model
 */
class Stock extends BaseModel
{
    public $table = 'module_stock';

    public $timestamps = false;

    public $fillable = [
        'id',
        'name',           //название склада
        'address',        //адрес склада
        'address_dadata', //json из dadata
        'city',           //город, где находится склад
        'street',         //улица
        'building',       //строение
        'storey',         //этаж
        'office',         //офис
        'contacts',       //строка с контактами
        'schedule',       //json с расписанием
        'company_id'      //id компании
    ];

    public $rules = [
        'name'           => 'required|min:1|max:255',
        'address'        => 'required|min:1|max:5000',
        'address_dadata' => 'required|min:1|max:50000',
        'city'           => 'required|min:1|max:255',
        'street'         => 'required|min:1|max:255',
        'building'       => 'required|min:1|max:255',
        'storey'         => 'required|min:1|max:255',
        'office'         => 'required|min:1|max:255',
        'contacts'       => 'required|min:1|max:5000',
        'schedule'       => 'required|min:1|max:5000',
        'company_id'     => 'required|integer|min:1|max:4294967295'
    ];

    public function company()
    {
        return $this->belongsTo('App\Modules\Companies\Model\Company', 'company_id');
    }

    public function remnants()
    {
        return $this->hasMany('App\Modules\Stock\Model\Remnants', 'stock_id');
    }
}


















