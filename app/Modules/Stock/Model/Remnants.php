<?php

namespace App\Modules\Stock\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с остатками на складах
 *
 * @package App\Modules\Companies\Model
 */
class Remnants extends BaseModel
{
    public $table = 'module_stock_remnants';

    public $timestamps = false;

    public $fillable = [
        'id',
        'stock_id', //id склада
        'sku_id',   //id торгового предложения
        'amount'    //количество товара
    ];

    public $rules = [
        'stock_id' => 'required|integer|min:1|max:4294967295',
        'sku_id'   => 'required|integer|min:1|max:4294967295',
        'amount'   => 'required|integer|min:0|max:4294967295',
    ];

    public function stock()
    {
        return $this->belongsTo('App\Modules\Stock\Model\Stock', 'stock_id');
    }

    public function sku()
    {
        return $this->belongsTo('App\Modules\Market\Model\SKU', 'sku_id');
    }
}


















