<?php
Route::group(
    [
        'namespace' => 'App\Modules\Stock\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => ['web','cors']
    ],
    function () {
        Route::get(   '/stock',      ['uses' => 'StockController@getStocks'      ]);
        Route::get(   '/stock/{id}', ['uses' => 'StockController@getStockById'   ]);

        Route::get(   '/remnant',      ['uses' => 'RemnantController@getRemnants'      ]);
        Route::get(   '/remnant/{id}', ['uses' => 'RemnantController@getRemnantById'   ]);

        Route::get('/product/{id}/remnant', ['uses' => 'RemnantController@getRemnantsByProductId']);
    }
);

Route::group(
    [
        'namespace' => 'App\Modules\Stock\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => [
            'web','auth','cors'
        ]
    ],
    function () {
        Route::get(   '/user/{user_id}/company/{company_id}/stock', ['uses' => 'StockController@getStocksByUserId']);
        Route::post(  '/stock',           ['uses' => 'StockController@postStock'        ]);
        Route::put(   '/stock',           ['uses' => 'StockController@putStock'         ]);
        Route::delete('/stock/{id}',      ['uses' => 'StockController@deleteStockById'  ]);

        Route::post(  '/remnant',      ['uses' => 'RemnantController@postRemnant'      ]);
        Route::put(   '/remnant',      ['uses' => 'RemnantController@putRemnant'       ]);
        Route::delete('/remnant/{id}', ['uses' => 'RemnantController@deleteRemnantById']);
    }
);
