<?php

namespace App\Modules\Stock\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Market\Controllers\ProductController;
use App\Modules\Market\Model\Product;
use App\Modules\Market\Model\SKU;
use App\Modules\Stock\Model\Remnants;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\Stock\Model\Stock;
use App\Modules\User\Controllers\UserController;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с остатками
 *
 * @package App\Modules\Remnant\Controllers
 */
class RemnantController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Stock';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет остаток
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function postRemnant(Request $request)
    {
        User::can('stock_postremnant', true);
        $data = $request->only(Remnants::fields());

        $Remnant = Remnants::where('stock_id', $data['stock_id'])
            ->where('sku_id', $data['sku_id'])->first();

        if ($Remnant) {
            $request->merge(['id' => $Remnant->id]);
            return $this->putRemnant($request);
        }

        $Remnant = Remnants::post($request, [
            'stock_id' => 'stock',
            'sku_id'   => 'sku'
        ]);

        if ($Remnant) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'stock_postremnant',
                null, 'remnant', $Remnant->id,
                ['data' => self::modelFilter($Remnant, Remnants::fields())]
            );
        }

        return parent::response($request->all(), $Remnant, 200);
    }

    /**
     * Изменяет остаток
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putRemnant(Request $request)
    {
        User::can('stock_putremnant', true);
        $Remnant = ['old' => false, 'new' => false];
        $Remnant = Remnants::put($request, [
            'stock_id' => 'stock',
            'sku_id'   => 'sku'
        ]);

        if (isset($Remnant['old']) && isset($Remnant['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'stock_putremnant',
                null, 'remnant', $Remnant['new']->id,
                ['data' => self::modelFilter($Remnant['new'], Remnants::fields())],
                [$Remnant['old'], $Remnant['new']]
            );
        }

        return parent::response($request->all(), $Remnant['new'], 200);
    }

    /**
     * Возвращает остаток по id
     *
     * @param int  $id   - id остатка
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getRemnantById($id, $json = true)
    {
        User::can('stock_viewremnant', true);

        $Remnant = Remnants::where('id', $id)->first();
        if (!$Remnant) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $Remnant;
        } else {
            return parent::response(['id' => $id], $Remnant, 200);
        }
    }

    /**
     * Возвращает остатки по всем складам и SKU для
     * товара с переданным id
     *
     * @param integer $id - id товара
     *
     * @return mixed
     */
    public function getRemnantsByProductId($id)
    {
        User::can('stock_viewremnant', true);
        $Remnants = Product::where('id', $id)
            ->with([
                'sku' => function($q) {
                    $q->with([
                        'remnants' => function ($q) {
                            $q->with('stock');
                        }
                    ]);
                }
            ])->first();

        $result = [];
        foreach ($Remnants->sku as $item) {
            if (count($item['remnants'])) {
                foreach ($item['remnants'] as $remnant) {
                    $result[] = [
                        'id'         => $remnant['id'],
                        'sku_id'     => $remnant['sku_id'],
                        'stock_id'   => $remnant['stock']['id'],
                        'amount'     => $remnant['amount'],
                    ];
                }
            }
        }
        return parent::response(['id' => $id], $result, 200);
    }

    /**
     * Возвращает остатки по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getRemnants(Request $request)
    {
        User::can('stock_viewremnant', true);
        $result = parent::dbGet(new Remnants(), $request, [], [
            'stock'   => [
                'model' => new Stock(),
                'fields' => ['id', 'name', 'city', 'address', 'company_id']
            ],
            'sku' => ['model' => new SKU(), 'fields' => SKU::fields()]
        ]);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет остаток по id
     *
     * @param int $id - id остатка
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteRemnantById($id)
    {
        User::can('stock_deleteremnant', true);

        $Remnant = Remnants::where('id', $id)->first();
        if (!$Remnant) {
            throw new CustomException(['id' => $id], [], 404);
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'stock_deleteremnant',
            null, 'remnant', $Remnant->id,
            ['data' => self::modelFilter($Remnant, Remnants::fields())]
        );

        return parent::response(['id' => $id], $Remnant->delete(), 200);

    }

}