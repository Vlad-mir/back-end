<?php

namespace App\Modules\Stock\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Model\Company;
use App\Modules\Stock\Model\Stock;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с складами
 *
 * @package App\Modules\Stock\Controllers
 */
class StockController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Stock';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый склад
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postStock(Request $request)
    {
        User::can('stock_poststock', true);
        $Stock = Stock::post($request, ['company_id' => 'company']);

        if ($Stock) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'stock_poststock',
                null, 'stock', $Stock->id,
                ['data' => self::modelFilter($Stock, Stock::fields())]
            );
        }

        return parent::response($request->all(), $Stock, 200);
    }

    /**
     * Изменяет существующий склад
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putStock(Request $request)
    {
        User::can('stock_putstock', true);
        $Stock = ['old' => false, 'new' => false];
        $Stock = Stock::put($request, ['company_id' => 'company']);

        if (isset($Stock['old']) && isset($Stock['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'stock_putstock',
                null, 'stock', $Stock['new']->id,
                ['data' => self::modelFilter($Stock['new'], Stock::fields())],
                [$Stock['old'], $Stock['new']]
            );
        }

        return parent::response($request->all(), $Stock['new'], 200);
    }

    /**
     * Возвращает склад по id
     *
     * @param int  $id   - id склада
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getStockById($id, $json = true)
    {
        User::can('stock_viewstock', true);

        $Stock = Stock::where('id', $id)->first();
        if (!$Stock) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $Stock;
        } else {
            return parent::response(['id' => $id], $Stock, 200);
        }
    }

    /**
     * Возвращает склады по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getStocks(Request $request)
    {
        User::can('stock_viewstock', true);
        $result = parent::dbGet(new Stock(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет склад по id
     *
     * @param int $id - id склада
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteStockById($id)
    {
        User::can('stock_deletestock', true);

        $Stock = Stock::where('id', $id)->first();
        if (!$Stock) {
            throw new CustomException(['id' => $id], [], 404);
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'stock_deletestock',
            null, 'stock', $Stock->id,
            ['data' => self::modelFilter($Stock, Stock::fields())]
        );

        return parent::response(['id' => $id], $Stock->delete(), 200);

    }


    /**
     * Возвращает все остатки на складе
     *
     * @param $stock_id
     *
     * @return mixed
     */
    public function getStockRemnants($stock_id)
    {
        $Stock = $this->getStockById($stock_id, false);

        return parent::response(
            ['stock_id' => $stock_id], $Stock->remnants()->get(), 200
        );
    }

    public function getStocksByUserId($userId, $companyId) {
        $User = User::where('id', $userId)
            ->with([
                'companies' => function ($q) use ($companyId) {
                    $q->where(Company::table().'.id', $companyId)->select(Company::table().'.id', 'name')->with([
                        'stocks' => function ($q) {
                            $q->select(Stock::table().'.id', 'name', 'company_id');
                        }
                    ]);
                }
            ])->first();
        $companies = []; $stocks = [];
        $companies = $User['companies'];
        if (count($companies)) {
            $stocks = $companies[0]['stocks'];
        }

        return parent::response(['user_id' => $userId], $stocks, 200);
    }

}