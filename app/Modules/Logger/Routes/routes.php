<?php
Route::group(
    [
        'namespace' => 'App\Modules\Logger\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => [
            'web'
        ]
    ],
    function () {

    }
);



Route::group(
    [
        'namespace' => 'App\Modules\Logger\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => [
            'web','auth'
        ]
    ],
    function () {
        Route::get('/events', ['uses' => 'LoggerController@getUserEvents']);
        Route::post('/log', ['uses' => 'LoggerController@writeApi']);
        Route::get('/log', ['uses' => 'LoggerController@getLog']);
        Route::delete('/log', ['uses' => 'LoggerController@removeAll']);
        Route::delete('/log/{id}', ['uses' => 'LoggerController@remove']);
    }
);
