<?php

namespace App\Modules\Billing\Model;

use App\Classes\BaseModel;
use App\Classes\MarketHelper as MH;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с способами оплаты заказа
 *
 * @package App\Modules\Billing\Model
 */
class PaymentType extends BaseModel
{
    public $table = 'module_billing_payment_type';

    public $timestamps = false;

    public $fillable = [
        'id',
        'name',        //название способа оплаты
        'description', //описание способа оплаты
    ];

    public $rules = [
        'name'        => 'required|min:1|max:255',
        'description' => 'required|min:1|max:50000'
    ];



}