<?php

namespace App\Modules\Billing\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с действиями над заказом
 *
 * @package App\Modules\Billing\Model
 */
class OrderAction extends BaseModel
{
    public $table = 'module_billing_order_action';

    public $timestamps = false;

    public $fillable = [
        'id',
        'name',        //название действия
        'description', //описание дейтсивя
        'code',        //символьный код действия
        'who'          //ключ-слово типа пользователя, который может выполнять действие
        //может принимать значения:
        //buyer         - Только покупатель
        //seller        - Только продавец
        //buyer||seller - Покупатель или продавец
        //moderator     - Только модератор или администратор
        //system        - Автоматическое действие
    ];

    public $rules = [
        'name'        => 'required|min:1|max:255',
        'description' => 'required|min:1|max:50000',
        'code'        => 'required|min:1|max:255',
        'who'         => 'required|min:1|max:255',
    ];



}