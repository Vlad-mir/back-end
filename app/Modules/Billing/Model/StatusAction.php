<?php

namespace App\Modules\Billing\Model;

use App\Classes\BaseModel;
use App\Exceptions\CustomException;
use App\Modules\Billing\Controllers\OrderActionController;
use App\Modules\Billing\Controllers\OrderStatusController;

/**
 * Модель для работы с отношениями между
 * статусами заказа и действиями над заказом
 *
 * @package App\Modules\Billing\Model
 */
class StatusAction extends BaseModel
{
    public $table = 'module_billing_order_status_action';

    public $timestamps = false;

    public $fillable = [
        'id',
        'status_id', //id статуса
        'action_id', //id действия
        'who',       //кто может выполнять действие в текущем статусе

    ];

    public $rules = [
        'status_id' => 'required|integer|min:1|max:4294967295',
        'action_id' => 'required|integer|min:1|max:4294967295',
        'who'       => 'required|min:1|max:255'
    ];

    /**
     * Типы пользователей, которые могут
     * совершать указанной действие над заказом
     *
     * @var array
     */
    public static $who = [
        'buyer',
        'seller',
        'system'
    ];



    /**
     * Связь со статусом
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Modules\Billing\Model\OrderStatus', 'status_id');
    }

    /**
     * Связь с действием
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function action()
    {
        return $this->belongsTo('App\Modules\Billing\Model\OrderAction', 'action_id');
    }


    /**
     * Прикрепляет доступные действия к статусу заказа
     *
     * @param $status_id - id статуса
     * @param $action_id - id действия
     * @param $who       - Код типа пользователя
     *
     * @return mixed
     * @throws CustomException
     */
    public static function attachActionStatus($status_id, $action_id, $who)
    {
        if (!in_array($who, self::$who)) {
            throw new CustomException([
                'status_id' => $status_id,
                'action_id' => $action_id,
                'who'       => $who
            ], [], 400, 'Некорректный тип пользователя');
        }

        $Status = OrderStatusController::call('getOrderStatusById', $status_id, false);
        $Action = OrderActionController::call('getOrderActionById', $action_id, false);
        $StatusAction = self::where('status_id', $Status->id)
            ->where('action_id', $Action->id)
            ->where('who', $who)->first();

        if (!$StatusAction) {
            self::create([
                'status_id' => $Status->id,
                'action_id' => $Action->id,
                'who'       => $who
            ]);
        }

        return OrderStatusController::call('getOrderStatusById', $status_id, false);
    }

}