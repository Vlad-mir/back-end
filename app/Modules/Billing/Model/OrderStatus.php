<?php

namespace App\Modules\Billing\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с действиями над заказом
 *
 * @package App\Modules\Billing\Model
 */
class OrderStatus extends BaseModel
{
    public $table = 'module_billing_order_status';

    public $timestamps = false;

    public $fillable = [
        'id',
        'name',        //название статуса
        'description', //описание статуса
        'code',        //символьный код статуса

    ];

    public $rules = [
        'name'        => 'required|min:1|max:255',
        'description' => 'required|min:1|max:50000',
        'code'        => 'required|min:1|max:255'
    ];

    /**
     * Связь статуса с действиями
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function actions()
    {
        return $this->belongsToMany(
            'App\Modules\Billing\Model\OrderAction',
            'module_billing_order_status_action', 'status_id', 'action_id'
        );
    }

    /**
     * Связь стстуса с действиями доступными для пользователей в данный момент
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accessActions()
    {
        return $this->hasMany('App\Modules\Billing\Model\StatusAction', 'status_id');
    }
}