<?php
//публичные методы
Route::group(
    [
        'namespace'  => 'App\Modules\Billing\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','cors']
    ],
    function () {

        /* Способы оплаты */

        Route::get('/payment_type',      ['uses' => 'PaymentTypeController@getPaymentTypes'  ]);
        Route::get('/payment_type/{id}', ['uses' => 'PaymentTypeController@getPaymentTypeById']);

        /* Действия над заказом */

        Route::get('/order_action',      ['uses' => 'OrderActionController@getOrderActions'  ]);
        Route::get('/order_action/{id}', ['uses' => 'OrderActionController@getOrderActionById']);

    }
);

//защищённые методы
Route::group(
    [
        'namespace'  => 'App\Modules\Billing\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','auth','cors']
    ],
    function () {

        /* Способы оплаты */

        Route::post(  '/payment_type',      ['uses' => 'PaymentTypeController@postPaymentType'      ]);
        Route::put(   '/payment_type',      ['uses' => 'PaymentTypeController@putPaymentType'       ]);
        Route::delete('/payment_type/{id}', ['uses' => 'PaymentTypeController@deletePaymentTypeById']);

        /* Действия над заказом */

        Route::post(  '/order_action',      ['uses' => 'OrderActionController@postOrderAction'      ]);
        Route::put(   '/order_action',      ['uses' => 'OrderActionController@putOrderAction'       ]);
        Route::delete('/order_action/{id}', ['uses' => 'OrderActionController@deleteOrderActionById']);

        /* Действия над статусами заказов */

        Route::get(   '/order_status',      ['uses' => 'OrderStatusController@getOrderStatuses'     ]);
        Route::get(   '/order_status/{id}', ['uses' => 'OrderStatusController@getOrderStatusById'   ]);
        Route::post(  '/order_status',      ['uses' => 'OrderStatusController@postOrderStatus'      ]);
        Route::put(   '/order_status',      ['uses' => 'OrderStatusController@putOrderStatus'       ]);
        Route::delete('/order_status/{id}', ['uses' => 'OrderStatusController@deleteOrderStatusById']);

        Route::post(
            '/attach_order_status',
            ['uses' => 'OrderStatusController@attachActionStatus']
        );
    }
);
