<?php

namespace App\Modules\Billing\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Billing\Model\OrderAction;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с типами доставки
 *
 * @package App\Modules\Billing\Controllers
 */
class OrderActionController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Billing';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новое действие над заказом
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postOrderAction(Request $request)
    {
        User::can('billing_postorderaction', true);
        $OrderAction = OrderAction::post($request);

        if ($OrderAction) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'billing_postorderaction',
                null, 'order_action', $OrderAction->id,
                ['data' => self::modelFilter($OrderAction, OrderAction::fields())]
            );
        }

        return parent::response($request->all(), $OrderAction, 200);
    }

    /**
     * Изменяет существующее действие над заказом
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putOrderAction(Request $request)
    {
        User::can('billing_putorderaction', true);
        $OrderAction = ['old' => false, 'new' => false];
        $OrderAction = OrderAction::put($request);

        if (isset($OrderAction['old']) && isset($OrderAction['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'billing_putorderaction',
                null, 'order_action', $OrderAction['new']->id,
                ['data' => self::modelFilter($OrderAction['new'], OrderAction::fields())],
                [$OrderAction['old'], $OrderAction['new']]
            );
        }

        return parent::response($request->all(), $OrderAction['new'], 200);
    }

    /**
     * Возвращает действие над заказом по id
     *
     * @param int  $id   - id действия над заказом
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getOrderActionById($id, $json = true)
    {
        User::can('billing_vieworderaction', true);

        $OrderAction = OrderAction::where('id', $id)->first();
        if (!$OrderAction) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $OrderAction;
        } else {
            return parent::response(['id' => $id], $OrderAction, 200);
        }
    }

    /**
     * Возвращает действия над заказом по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getOrderActions(Request $request)
    {
        User::can('billing_vieworderaction', true);
        $result = parent::dbGet(new OrderAction(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет действие над заказом по id
     *
     * @param int $id - id действия над заказом
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteOrderActionById($id)
    {
        User::can('billing_deleteorderaction', true);

        $OrderAction = $this->getOrderActionById($id, false);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'billing_deleteorderaction',
            null, 'order_action', $OrderAction->id,
            ['data' => self::modelFilter($OrderAction, OrderAction::fields())]
        );

        return parent::response(['id' => $id], $OrderAction->delete(), 200);

    }
}