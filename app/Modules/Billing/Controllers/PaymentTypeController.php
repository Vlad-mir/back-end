<?php

namespace App\Modules\Billing\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Billing\Model\PaymentType;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с типами доставки
 *
 * @package App\Modules\Billing\Controllers
 */
class PaymentTypeController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Billing';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый способ оплаты
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postPaymentType(Request $request)
    {
        User::can('billing_postbillingtype', true);
        $PaymentType = PaymentType::post($request);

        if ($PaymentType) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'billing_postbillingtype',
                null, 'payment_type', $PaymentType->id,
                ['data' => self::modelFilter($PaymentType, PaymentType::fields())]
            );
        }

        return parent::response($request->all(), $PaymentType, 200);
    }

    /**
     * Изменяет существующий способ оплаты
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putPaymentType(Request $request)
    {
        User::can('billing_putbillingtype', true);
        $PaymentType = ['old' => false, 'new' => false];
        $PaymentType = PaymentType::put($request);

        if (isset($PaymentType['old']) && isset($PaymentType['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'billing_putbillingtype',
                null, 'payment_type', $PaymentType['new']->id,
                ['data' => self::modelFilter($PaymentType['new'], PaymentType::fields())],
                [$PaymentType['old'], $PaymentType['new']]
            );
        }

        return parent::response($request->all(), $PaymentType['new'], 200);
    }

    /**
     * Возвращает способ оплаты по id
     *
     * @param int  $id   - id способа оплаты
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getPaymentTypeById($id, $json = true)
    {
        User::can('billing_viewbillingtype', true);

        $PaymentType = PaymentType::where('id', $id)->first();
        if (!$PaymentType) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $PaymentType;
        } else {
            return parent::response(['id' => $id], $PaymentType, 200);
        }
    }

    /**
     * Возвращает способы оплаты по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getPaymentTypes(Request $request)
    {
        User::can('billing_viewbillingtype', true);
        $result = parent::dbGet(new PaymentType(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет способ оплаты по id
     *
     * @param int $id - id способа оплаты
     *
     * @return mixed
     * @throws CustomException
     */
    public function deletePaymentTypeById($id)
    {
        User::can('billing_deletebillingtype', true);

        $PaymentType = $this->getPaymentTypeById($id, false);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'billing_deletebillingtype',
            null, 'payment_type', $PaymentType->id,
            ['data' => self::modelFilter($PaymentType, PaymentType::fields())]
        );

        return parent::response(['id' => $id], $PaymentType->delete(), 200);

    }
}