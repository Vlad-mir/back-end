<?php

namespace App\Modules\Billing\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Billing\Model\OrderAction;
use App\Modules\Billing\Model\OrderStatus;
use App\Modules\Billing\Model\StatusAction;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с статусами заказа
 *
 * @package App\Modules\Billing\Controllers
 */
class OrderStatusController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Billing';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый статус заказа
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postOrderStatus(Request $request)
    {
        User::can('billing_postorderstatus', true);
        $OrderStatus = OrderStatus::post($request);

        if ($OrderStatus) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'billing_postorderstatus',
                null, 'order_status', $OrderStatus->id,
                ['data' => self::modelFilter($OrderStatus, OrderStatus::fields())]
            );
        }

        return parent::response($request->all(), $OrderStatus, 200);
    }

    /**
     * Изменяет существующий статус заказа
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putOrderStatus(Request $request)
    {
        User::can('billing_putorderstatus', true);
        $OrderStatus = ['old' => false, 'new' => false];
        $OrderStatus = OrderStatus::put($request);

        if (isset($OrderStatus['old']) && isset($OrderStatus['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'billing_putorderstatus',
                null, 'order_status', $OrderStatus['new']->id,
                ['data' => self::modelFilter($OrderStatus['new'], OrderStatus::fields())],
                [$OrderStatus['old'], $OrderStatus['new']]
            );
        }

        return parent::response($request->all(), $OrderStatus['new'], 200);
    }

    /**
     * Возвращает статус заказа по id
     *
     * @param int  $id   - id статуса заказа
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getOrderStatusById($id, $json = true)
    {
        User::can('billing_vieworderstatus', true);

        $OrderStatus = OrderStatus::where('id', $id)
            ->with([
                'actions',
                'accessActions' => function ($q) {
                    $q->with(['action']);
                }
            ])->first();
        if (!$OrderStatus) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $OrderStatus;
        } else {
            return parent::response(['id' => $id], $OrderStatus, 200);
        }
    }

    /**
     * Возвращает статусы заказа по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getOrderStatuses(Request $request)
    {
        User::can('billing_vieworderstatus', true);
        $result = parent::dbGet(new OrderStatus(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет статус заказа по id
     *
     * @param int $id - id статуса заказа
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteOrderStatusById($id)
    {
        User::can('billing_deleteorderstatus', true);

        $OrderStatus = $this->getOrderStatusById($id, false);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'billing_deleteorderstatus',
            null, 'order_status', $OrderStatus->id,
            ['data' => self::modelFilter($OrderStatus, OrderStatus::fields())]
        );

        return parent::response(['id' => $id], $OrderStatus->delete(), 200);

    }

    /**
     * Прикрепляет доступные действия к статусу заказа
     *
     * @param Request $request - запрос от клиента
     *
     * @return mixed
     */
    public function attachActionStatus(Request $request)
    {
        User::can('billing_attachorderactiontostatus', true);

        $result = StatusAction::attachActionStatus(
            $request->get('status_id'),
            $request->get('action_id'),
            $request->get('who')
        );

        return parent::response($request->all(), $result, 200);
    }
}