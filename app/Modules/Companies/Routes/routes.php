<?php
Route::group(
    [
        'namespace' => 'App\Modules\Companies\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => ['web','cors']
    ],
    function () {}
);

Route::group(
    [
        'namespace' => 'App\Modules\Companies\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => [
            'web','auth','cors'
        ]
    ],
    function () {
        Route::post(  '/companies',           ['uses' => 'CompanyController@postCompany'      ]);
        Route::put(   '/companies',           ['uses' => 'CompanyController@putCompany'       ]);
        Route::get(   '/companies',           ['uses' => 'CompanyController@getCompanies'     ]);
        Route::get(   '/companies/{id}',      ['uses' => 'CompanyController@getCompanyById'   ]);
        Route::delete('/companies/{id}',      ['uses' => 'CompanyController@deleteCompanyById']);
        Route::get(   '/user/{id}/companies', ['uses' => 'CompanyController@getUserCompanies' ]);
    }
);
