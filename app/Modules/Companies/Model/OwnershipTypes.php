<?php

namespace App\Modules\Companies\Model;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomValidationException;
use App\Interfaces\ModuleModelInterface;
use App\Modules\Module\Model\Module;
use App\Exceptions\CustomDBException;
use App\Classes\BaseModel;

/**
 * Class OwnershipTypes - Модель для работы с типами юридических лиц
 *
 * @package App\Modules\Companies\Model
 */
class OwnershipTypes extends BaseModel
{
    public $table = 'module_companies_ownership_types';

    public $fillable = [
        'id',
        'name'
    ];

    public $rules = [
        'name'              => 'required|min:1|max:255'
    ];
}


















