<?php

namespace App\Modules\Companies\Model;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomValidationException;
use App\Interfaces\ModuleModelInterface;
use App\Modules\Module\Model\Module;
use App\Exceptions\CustomDBException;
use App\Classes\BaseModel;

/**
 * Class Company - Модель для работы с компаниями
 *
 * @package App\Modules\Companies\Model
 */
class Company extends BaseModel
{
    public $table = 'module_companies';

    public $fillable = [
        'id',
        'name',              //название компании
        'email',             //общий email компании
        'inn',               //ИНН компании
        'ownership_type_id', //Форма собственности
        'tax_system_id',     //система налогообложения
        'vat_id'             //НДС
    ];

    public $rules = [
        'name'              => 'required|min:1|max:255',
        'email'             => 'email|required|min:1|max:255',
        'inn'               => 'required|min:1|max:255',
        'ownership_type_id' => 'required|integer|min:1|max:4294967295',
        'tax_system_id'     => 'required|integer|min:1|max:4294967295',
        'vat_id'            => 'required|integer|min:1|max:4294967295'
    ];

    public function ownershipType()
    {
        return $this->belongsTo('App\Modules\Companies\Model\OwnershipTypes', 'ownership_type_id');
    }

    public function taxSystem()
    {
        return $this->belongsTo('App\Modules\Companies\Model\TaxSystems', 'tax_system_id');
    }

    public function vat()
    {
        return $this->belongsTo('App\Modules\Companies\Model\Vat', 'vat_id');
    }

    public function stocks()
    {
        return $this->hasMany('App\Modules\Stock\Model\Stock', 'company_id');
    }

    public function users()
    {
        return $this->belongsToMany(
            'App\Modules\User\Model\User',
            'module_companies_users', 'company_id', 'user_id'
        );
    }

    public function products()
    {
        return $this->hasMany('App\Modules\Market\Model\Product', 'company_id');
    }
}


















