<?php

namespace App\Modules\Companies\Model;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomValidationException;
use App\Interfaces\ModuleModelInterface;
use App\Modules\Module\Model\Module;
use App\Exceptions\CustomDBException;
use App\Classes\BaseModel;

/**
 * Class TaxSystems - Модель для работы с системами налогооблажения
 *
 * @package App\Modules\Companies\Model
 */
class TaxSystems extends BaseModel
{
    public $table = 'module_companies_tax_systems';

    public $fillable = [
        'id',
        'name',
        'code'
    ];

    public $rules = [
        'name' => 'required|min:1|max:255',
        'code' => 'required|min:1|max:255'
    ];
}


















