<?php

namespace App\Modules\Companies\Model;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomValidationException;
use App\Interfaces\ModuleModelInterface;
use App\Modules\Module\Model\Module;
use App\Exceptions\CustomDBException;
use App\Classes\BaseModel;

/**
 * Class Vat - Модель для работы с налоговыми ставками
 *
 * @package App\Modules\Companies\Model
 */
class Vat extends BaseModel
{
    public $table = 'module_companies_vat';

    public $fillable = [
        'id',
        'name',
        'value'
    ];

    public $rules = [
        'name' => 'required|min:1|max:255',
        'value' => 'required|integer|min:0|max:100'
    ];
}


















