<?php

namespace App\Modules\Companies\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Model\OwnershipTypes;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с типами прав собственности
 *
 * @package App\Modules\Companies\Controllers
 */
class OwnershipTypesController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Companies';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый тип прав собственности
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postOwnershipType(Request $request)
    {
        User::can('companies_postownershiptype', true);
        $OwnershipTypes = OwnershipTypes::post($request);

        if ($OwnershipTypes) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'companies_postownershiptype',
                null, 'companies', $OwnershipTypes->id,
                ['data' => self::modelFilter($OwnershipTypes, OwnershipTypes::fields())]
            );
        }

        return parent::response($request->all(), $OwnershipTypes, 200);
    }

    /**
     * Изменяет существующую тип прав собственности
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putOwnershipType(Request $request)
    {
        User::can('companies_putownershiptype', true);
        $OwnershipTypes = ['old' => false, 'new' => false];
        $OwnershipTypes = OwnershipTypes::put($request);

        if (isset($OwnershipTypes['old']) && isset($OwnershipTypes['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'companies_putownershiptype',
                null, 'companies', $OwnershipTypes['new']->id,
                ['data' => self::modelFilter($OwnershipTypes['new'], OwnershipTypes::fields())],
                [$OwnershipTypes['old'], $OwnershipTypes['new']]
            );
        }

        return parent::response($request->all(), $OwnershipTypes['new'], 200);
    }

    /**
     * Возвращает тип прав собственности по id
     *
     * @param int  $id   - id компании
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getOwnershipTypeById($id, $json = true)
    {
        User::can('companies_viewownershiptype', true);

        $OwnershipTypes = OwnershipTypes::where('id', $id)->first();
        if (!$OwnershipTypes) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $OwnershipTypes;
        } else {
            return parent::response(['id' => $id], $OwnershipTypes, 200);
        }
    }

    /**
     * Возвращает типы прав собственности по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getOwnershipTypes(Request $request)
    {
        User::can('companies_viewownershiptype', true);
        $result = parent::dbGet(new OwnershipTypes(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет тип прав собственности по id
     *
     * @param int $id - id компании
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteOwnershipTypeById($id)
    {
        User::can('companies_deleteownershiptype', true);

        $OwnershipTypes = OwnershipTypes::where('id', $id)->first();
        if (!$OwnershipTypes) {
            throw new CustomException(['id' => $id], [], 404);
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'companies_deleteownershiptype',
            null, 'companies', $OwnershipTypes->id,
            ['data' => self::modelFilter($OwnershipTypes, OwnershipTypes::fields())]
        );

        return parent::response(['id' => $id], $OwnershipTypes->delete(), 200);

    }

}