<?php

namespace App\Modules\Companies\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\State;
use App\Modules\Companies\Model\Company;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Controllers\UserController;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с компаниями
 *
 * @package App\Modules\Companies\Controllers
 */
class CompanyController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Companies';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новую компанию
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postCompany(Request $request)
    {
        User::can('companies_postcompany', true);
        $Company = Company::post($request, [
            'ownership_type_id' => 'ownershipType',
            'tax_system_id' => 'taxSystem',
            'vat_id' => 'vat'
        ]);

        if ($Company) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'companies_postcompany',
                null, 'companies', $Company->id,
                ['data' => self::modelFilter($Company, Company::fields())]
            );
        }

        return parent::response($request->all(), $Company, 200);
    }

    /**
     * Изменяет существующую компанию
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putCompany(Request $request)
    {
        User::can('companies_putcompany', true);
        $Company = ['old' => false, 'new' => false];
        $Company = Company::put($request, [
            'ownership_type_id' => 'ownershipType',
            'tax_system_id' => 'taxSystem',
            'vat_id' => 'vat'
        ]);

        if (isset($Company['old']) && isset($Company['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'companies_putcompany',
                null, 'companies', $Company['new']->id,
                ['data' => self::modelFilter($Company['new'], Company::fields())],
                [$Company['old'], $Company['new']]
            );
        }

        return parent::response($request->all(), $Company['new'], 200);
    }

    /**
     * Возвращает компанию по id
     *
     * @param int  $id   - id компании
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getCompanyById($id, $json = true)
    {
        User::can('companies_viewcompany', true);

        $Company = Company::where('id', $id)->first();
        if (!$Company) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $Company;
        } else {
            return parent::response(['id' => $id], $Company, 200);
        }
    }

    /**
     * Возвращает компании по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getCompanies(Request $request)
    {
        User::can('companies_viewcompany', true);
        $result = parent::dbGet(new Company(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет компанию по id
     *
     * @param int $id - id компании
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteCompanyById($id)
    {
        User::can('companies_deletecompany', true);

        $Company = Company::where('id', $id)->first();
        if (!$Company) {
            throw new CustomException(
                ['id' => $id], [], 404, 'Компания с id ' . $id . ' не найдена'
            );
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'companies_deletecompany',
            null, 'companies', $Company->id,
            ['data' => self::modelFilter($Company, Company::fields())]
        );

        return parent::response(['id' => $id], $Company->delete(), 200);

    }

    /**
     * Получение компаний, в которых участвует пользователь
     *
     * @param null|int $id   - id пользователя
     * @param boolean  $json - формат ответа
     *
     * @return mixed
     * @throws CustomException
     */
    public function getUserCompanies($id = null, $json = true)
    {
        if ($id === null) {
            if (!$User = State::User()) {
                throw new CustomException(['id' => $id], [], 404, 'Требуется указать id пользователя');
            }
            $id = $User['id'];
        }
        $User = UserController::call('getUserById', $id, true);
        $result = $User->companies()->get();
        return ($json) ? parent::response(['id' => $id], $result, 200) : $result;
    }
}