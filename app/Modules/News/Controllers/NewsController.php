<?php

namespace App\Modules\News\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Model\Company;
use App\Modules\Logger\Model\Logger;
use App\Modules\News\Model\News;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\Properties\Controllers\FileController;
use App\Modules\Properties\Model\Files;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с категорями
 *
 * @package App\Modules\News\Controllers
 */
class NewsController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'News';

    public $types = [
        'news', 'action'
    ];

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новую категорию
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postNews(Request $request)
    {
        User::can('news_postnews', true);

        //добавляем файл
        if ($request->hasFile('hero_image')) {
            $file = self::upFile($request, 'hero_image');
            $request->merge(['hero_image_id' => $file['id']]);
        }

        $News = News::post($request, ['company_id' => 'company']);

        if ($News) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'news_postnews',
                null, 'news', $News->id,
                ['data' => self::modelFilter($News, News::fields())]
            );
        }

        return parent::response($request->all(), $News, 200);
    }

    /**
     * Изменяет существующую категорию
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putNews(Request $request)
    {
        User::can('news_putnews', true);
        $News = ['old' => false, 'new' => false];

        if ($request->hasFile('hero_image')) {
            $file = self::upFile($request, 'hero_image');
            $request->merge(['hero_image_id' => $file['id']]);
        }

        $News = News::put($request, ['company_id' => 'company']);

        if (isset($News['old']) && isset($News['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'news_putnews',
                null, 'news', $News['new']->id,
                ['data' => self::modelFilter($News['new'], News::fields())],
                [$News['old'], $News['new']]
            );
            //удаляем старый файл
            FileController::call('deleteFileById', $News['old']['hero_image_id'], false);
        }

        return parent::response($request->all(), $News['new'], 200);
    }

    /**
     * Возвращает категорию по id
     *
     * @param int  $id   - id категории
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getNewsById($id, $json = true)
    {
        User::can('news_viewnews', true);

        $News = News::where('id', $id)
            ->with('company', 'heroImage')->first();
        if (!$News) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $News;
        } else {
            return parent::response(['id' => $id], $News, 200);
        }
    }

    /**
     * Возвращает категории по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getAllNews(Request $request)
    {
        User::can('news_viewnews', true);
        $result = parent::dbGet(new News(), $request, [], [
            'company'   => new Company(),
            'heroImage' => new Files()
        ]);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет категории по id
     *
     * @param int $id - id кстегории
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteNewsById($id)
    {
        User::can('news_deletenews', true);

        $News = News::where('id', $id)->first();
        if (!$News) {
            throw new CustomException(['id' => $id], [], 404);
        }

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'news_deletenews',
            null, 'news', $News->id,
            ['data' => self::modelFilter($News, News::fields())]
        );

        //удаляем файл картинки
        FileController::call('deleteFileById', $News->hero_image_id, false);

        //удаляем id у лога
        $Events = Logger::where('log_action','news_postnews')
            ->where('entity_id', $id)
            ->where('entity_type', 'news')->first();
        if ($Events) {
            $Events->entity_id = null;
            $Events->save();
        }

        return parent::response(['id' => $id], $News->delete(), 200);

    }

}