<?php

namespace App\Modules\News\Model;

use App\Classes\BaseModel;

/**
 * Модель для работы с категорями магазина
 *
 * @package App\Modules\News\Model
 */
class News extends BaseModel
{
    public $table = 'module_news';

    public $timestamps = true;

    public $fillable = [
        'id',
        'title',
        'description',
        'keywords',
        'company_id',
        'content',
        'hero_image_id',
        'type',
        'code',
        'moderate', //0 - снята с публикации, 1 - опубликована, 2 - на модерации, 3 - отклонена модеротором
        'to_date',
        'created_at',
        'updated_at'
    ];

    public $rules = [
        'title'         => 'required|min:1|max:255',
        'code'          => 'required|min:1|max:255|unique:module_news,code',
        'description'   => 'nullable|min:0|max:500',
        'keywords'      => 'nullable|min:0|max:500',
        'company_id'    => 'required|integer|min:0|max:999',
        'content'       => 'required|min:1|max:500000',
        'type'          => 'required|min:1|max:500',
        'moderate'      => 'required|integer|min:1|max:500',
        'to_date'       => 'nullable|date_format:Y-m-d G:i:s',
        'hero_image_id' => 'required|integer|min:1|max:4294967295'
    ];

    /**
     * Связь с изображением
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function heroImage()
    {
        return $this->belongsTo('App\Modules\Properties\Model\Files', 'hero_image_id');
    }

    /**
     * Связь с компанией
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Modules\Companies\Model\Company', 'company_id');
    }

}