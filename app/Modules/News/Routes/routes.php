<?php
Route::group(
    [
        'namespace' => 'App\Modules\News\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => ['web','cors']
    ],
    function () {
        Route::get('/news',      ['uses' => 'NewsController@getAllNews']);
        Route::get('/news/{id}', ['uses' => 'NewsController@getNews'   ]);
    }
);

Route::group(
    [
        'namespace' => 'App\Modules\News\Controllers',
        'as' => 'module.',
        'prefix' => 'api',
        'middleware' => [
            'web','auth','cors'
        ]
    ],
    function () {
        Route::post(  '/news',      ['uses' => 'NewsController@postNews'      ]);
        Route::put(   '/news',      ['uses' => 'NewsController@putNews'       ]);
        Route::delete('/news/{id}', ['uses' => 'NewsController@deleteNewsById']);
    }
);
