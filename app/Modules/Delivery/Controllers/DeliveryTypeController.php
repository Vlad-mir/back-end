<?php

namespace App\Modules\Delivery\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Delivery\Model\DeliveryType;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с типами доставки
 *
 * @package App\Modules\Delivery\Controllers
 */
class DeliveryTypeController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Delivery';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новый тип доставки
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postDeliveryType(Request $request)
    {
        User::can('delivery_postdeliverytype', true);
        $DeliveryType = DeliveryType::post($request);

        if ($DeliveryType) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'delivery_postdeliverytype',
                null, 'delivery_type', $DeliveryType->id,
                ['data' => self::modelFilter($DeliveryType, DeliveryType::fields())]
            );
        }

        return parent::response($request->all(), $DeliveryType, 200);
    }

    /**
     * Изменяет существующий тип доставки
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putDeliveryType(Request $request)
    {
        User::can('delivery_putdeliverytype', true);
        $DeliveryType = ['old' => false, 'new' => false];
        $DeliveryType = DeliveryType::put($request);

        if (isset($DeliveryType['old']) && isset($DeliveryType['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'delivery_putdeliverytype',
                null, 'delivery_type', $DeliveryType['new']->id,
                ['data' => self::modelFilter($DeliveryType['new'], DeliveryType::fields())],
                [$DeliveryType['old'], $DeliveryType['new']]
            );
        }

        return parent::response($request->all(), $DeliveryType['new'], 200);
    }

    /**
     * Возвращает тип доставки по id
     *
     * @param int  $id   - id типа доставки
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getDeliveryTypeById($id, $json = true)
    {
        User::can('delivery_viewdeliverytype', true);

        $DeliveryType = DeliveryType::where('id', $id)->first();
        if (!$DeliveryType) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $DeliveryType;
        } else {
            return parent::response(['id' => $id], $DeliveryType, 200);
        }
    }

    /**
     * Возвращает типы доставки по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getDeliveryTypes(Request $request)
    {
        User::can('delivery_viewdeliverytype', true);
        $result = parent::dbGet(new DeliveryType(), $request);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет тип доставки по id
     *
     * @param int $id - id типа доставки
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteDeliveryTypeById($id)
    {
        User::can('delivery_deletedeliverytype', true);

        $DeliveryType = $this->getDeliveryTypeById($id, false);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'delivery_deletedeliverytype',
            null, 'delivery_type', $DeliveryType->id,
            ['data' => self::modelFilter($DeliveryType, DeliveryType::fields())]
        );

        return parent::response(['id' => $id], $DeliveryType->delete(), 200);

    }

}