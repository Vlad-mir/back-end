<?php

namespace App\Modules\Delivery\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Companies\Model\Company;
use App\Modules\Delivery\Model\CompanyDelivery;
use App\Modules\Delivery\Model\DeliveryType;
use App\Modules\Logger\Controllers\LoggerController;
use App\Interfaces\ModuleInterface;
use App\Exceptions\CustomException;
use App\Modules\User\Model\User;
use Illuminate\Http\Request;

/**
 * Класс для работы с доставками компаний
 *
 * @package App\Modules\Delivery\Controllers
 */
class CompanyDeliveryController extends Controller implements ModuleInterface
{
    /**
     * Название модуля
     *
     * @var string
     */
    public $moduleName = 'Delivery';

    /**
     * Вернёт код модуля
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Добавляет новую доставку компании
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function postCompanyDelivery(Request $request)
    {
        User::can('delivery_postcompanydelivery', true);
        $CompanyDelivery = CompanyDelivery::post($request, [
            'company_id' => 'company',
            'type_id'    => 'type'
        ]);

        if ($CompanyDelivery) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'delivery_postcompanydelivery',
                null, 'company_delivery', $CompanyDelivery->id,
                ['data' => self::modelFilter($CompanyDelivery, CompanyDelivery::fields())]
            );
        }

        return parent::response($request->all(), $CompanyDelivery, 200);
    }

    /**
     * Изменяет существующую доставку
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     * @throws CustomException
     */
    public function putCompanyDelivery(Request $request)
    {
        User::can('delivery_putcompanydelivery', true);
        $CompanyDelivery = ['old' => false, 'new' => false];
        $CompanyDelivery = CompanyDelivery::put($request, [
            'company_id' => 'company',
            'type_id'    => 'type'
        ]);

        if (isset($CompanyDelivery['old']) && isset($CompanyDelivery['new'])) {
            //логируем действие
            LoggerController::write(
                $this->getModuleName(), 'delivery_putcompanydelivery',
                null, 'company_delivery', $CompanyDelivery['new']->id,
                ['data' => self::modelFilter($CompanyDelivery['new'], CompanyDelivery::fields())],
                [$CompanyDelivery['old'], $CompanyDelivery['new']]
            );
        }

        return parent::response($request->all(), $CompanyDelivery['new'], 200);
    }

    /**
     * Возвращает доставку по id
     *
     * @param int  $id   - id доставки
     * @param bool $json - флаг отправки json
     *
     * @return mixed
     * @throws CustomException
     */
    public function getCompanyDeliveryById($id, $json = true)
    {
        User::can('delivery_viewcompanydelivery', true);

        $CompanyDelivery = CompanyDelivery::where('id', $id)
            ->with(['company', 'type'])->first();
        if (!$CompanyDelivery) {
            throw new CustomException(['id' => $id], [], 404);
        }

        if (!$json) {
            return $CompanyDelivery;
        } else {
            return parent::response(['id' => $id], $CompanyDelivery, 200);
        }
    }

    /**
     * Возвращает доставки компании по указанному фильтру
     *
     * @param Request $request - Запрос от клиента
     *
     * @return mixed
     */
    public function getCompanyDeliveries(Request $request)
    {
        User::can('delivery_viewcompanydelivery', true);
        $result = parent::dbGet(new CompanyDelivery(), $request, [], [
            'company' => new Company(),
            'type'    => new DeliveryType()
        ]);
        return parent::response($request->all(), $result, 200);
    }

    /**
     * Удаляет доставку по id
     *
     * @param int $id - id доставки
     *
     * @return mixed
     * @throws CustomException
     */
    public function deleteCompanyDeliveryById($id)
    {
        User::can('delivery_deletecompanydelivery', true);

        $CompanyDelivery = $this->getCompanyDeliveryById($id, false);

        //логируем действие
        LoggerController::write(
            $this->getModuleName(), 'delivery_deletecompanydelivery',
            null, 'company_delivery', $CompanyDelivery->id,
            ['data' => self::modelFilter($CompanyDelivery, CompanyDelivery::fields())]
        );

        return parent::response(['id' => $id], $CompanyDelivery->delete(), 200);

    }

}