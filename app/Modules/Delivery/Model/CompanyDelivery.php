<?php

namespace App\Modules\Delivery\Model;

use App\Classes\BaseModel;
use App\Classes\MarketHelper as MH;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с доставкой у компаний
 *
 * @package App\Modules\Market\Model
 */
class CompanyDelivery extends BaseModel
{
    public $table = 'module_delivery_company';

    public $timestamps = false;

    public $fillable = [
        'id',
        'city',       //Название города
        'condition',  //Условие доставки (на прмимер: В пределах города)
        'weight',     //Максимальный вес доставки
        'price',      //Цена
        'contacts',   //Контакты
        'company_id', //id компании
        'type_id'     //id типа доставки
    ];

    public $rules = [
        'city'       => 'required|min:1|max:255',
        'condition'  => 'required|min:1|max:255',
        'weight'     => 'required|min:1|max:255',
        'price'      => 'required|numeric|min:0|max:4294967295',
        'contacts'   => 'required|min:1|max:50000',
        'company_id' => 'required|integer|min:0|max:4294967295',
        'type_id'    => 'required|integer|min:0|max:4294967295'
    ];

    /**
     * Связь с компанией
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('App\Modules\Companies\Model\Company', 'company_id');
    }

    /**
     * Связь с типом доставки
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Modules\Delivery\Model\DeliveryType', 'type_id');
    }
}