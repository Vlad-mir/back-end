<?php

namespace App\Modules\Delivery\Model;

use App\Classes\BaseModel;
use App\Classes\MarketHelper as MH;
use App\Exceptions\CustomDBException;
use App\Exceptions\CustomException;

/**
 * Модель для работы с типами доставки
 *
 * @package App\Modules\Market\Model
 */
class DeliveryType extends BaseModel
{
    public $table = 'module_delivery_type';

    public $timestamps = true;

    public $fillable = [
        'id',
        'name',        //название типа доставки
        'description', //описание типа доставки
    ];

    public $rules = [
        'name'        => 'required|min:1|max:255',
        'description' => 'nullable|min:0|max:50000'
    ];
}