<?php
//публичные методы
Route::group(
    [
        'namespace'  => 'App\Modules\Delivery\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','cors']
    ],
    function () {

        /* Типы доставки товаров */

        Route::get('/delivery_type',      ['uses' => 'DeliveryTypeController@getDeliveryTypes'  ]);
        Route::get('/delivery_type/{id}', ['uses' => 'DeliveryTypeController@getDeliveryTypeById']);

        /* Вариаты доставки товаров у компании */

        Route::get('/delivery_company',      ['uses' => 'CompanyDeliveryController@getCompanyDeliveries'  ]);
        Route::get('/delivery_company/{id}', ['uses' => 'CompanyDeliveryController@getCompanyDeliveryById']);
    }
);

//защищённые методы
Route::group(
    [
        'namespace'  => 'App\Modules\Delivery\Controllers',
        'as'         => 'module.',
        'prefix'     => 'api',
        'middleware' => ['web','auth','cors']
    ],
    function () {

        /* Типы доставки товаров */

        Route::post(  '/delivery_type',      ['uses' => 'DeliveryTypeController@postDeliveryType'      ]);
        Route::put(   '/delivery_type',      ['uses' => 'DeliveryTypeController@putDeliveryType'       ]);
        Route::delete('/delivery_type/{id}', ['uses' => 'DeliveryTypeController@deleteDeliveryTypeById']);

        /* Вариаты доставки товаров у компании */

        Route::post(  '/delivery_company',      ['uses' => 'CompanyDeliveryController@postCompanyDelivery'      ]);
        Route::put(   '/delivery_company',      ['uses' => 'CompanyDeliveryController@putCompanyDelivery'       ]);
        Route::delete('/delivery_company/{id}', ['uses' => 'CompanyDeliveryController@deleteCompanyDeliveryById']);
    }
);
