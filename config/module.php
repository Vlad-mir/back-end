<?php
return [
	'modules' => [
		'module',
        'user',
        'logger',
        'properties',
        'comments',
        'companies',
        'stock',
        'market',
        'delivery',
        'billing',
        'integration',
        'news'
	],
    'require_modules' => [
        'module',
        'user',
        'logger',
        'properties',
        'comments',
        'companies',
        'stock',
        'market',
        'delivery',
        'billing',
        'integration',
        'news'
    ],
    'module_dir' => __DIR__.'/../app/Modules',
];